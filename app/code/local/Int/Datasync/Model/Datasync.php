<?php

class Int_Datasync_Model_Datasync extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('datasync/datasync');
    }
}