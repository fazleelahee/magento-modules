<?php

/**
 * Class Int_Datasync_Model_ProductionSoap
 * Response for updating products/ category and attribute to destinations soap server.
 */
class Int_Datasync_Model_Productionsoapv2 extends Int_Datasync_Model_Productionsoap {

    protected static $clientv2  = null;
    protected $sessionv2        = null;

    protected $soapV2ArrayMap = [
        'product_attribute.create'  => 'catalogProductAttributeCreate',
        'product_attribute.info'    => 'catalogProductAttributeInfo',
        'product_attribute.update'  => 'catalogProductAttributeUpdate'
    ];

    public function __construct() {
        // Connection to Production
        $prodSoapUrl = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapiurl');
        $arrUrl = parse_url($prodSoapUrl);
        $prodSoapUrl = "{$arrUrl['scheme']}://{$arrUrl['host']}/api/v2_soap/?wsdl";

        if(null === static::$clientv2) {
            static::$clientv2 = new SoapClient($prodSoapUrl);
        }

        $this->dataSyncLogFile = sprintf($this->dataSyncLogFile, date('Y'),date('m'), date('d'));
        $this->getSession();
        return $this;
    }

    public function getSession() {
        try {
            $prodSoapUsername = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapiusername');
            $prodSoapPassword = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapipass');

            if($this->sessionv2 == null) {
                $this->sessionv2 = static::$client->login($prodSoapUsername, $prodSoapPassword);
            }
        } catch (SoapFault $fault) {
            $message = sprintf('SOAP Fault# Code:%s, Message:%s',$fault->faultcode, $fault->faultstring);
            $this->log($message, Zend_Log::ERR);
            throw new Exception ($message);
        }
        return $this;
    }

    public function send($method, $args='') {
        try {
            if(is_array($args) || $args != '') {
                if(isset($this->soapV2ArrayMap[$method])) {
                    $function = $this->soapV2ArrayMap[$method];
                    if(isset($args['id']) && isset($args['data'])) {
                        $response = static::$clientv2->$function($this->sessionv2, $args['id'], $args['data']);
                    } else {
                        $response = static::$clientv2->$function($this->sessionv2, $args);
                    }
                    $this->noOfTries = 0;
                    return $response;
                }
            }

            if(isset($this->soapV2ArrayMap[$method])) {
                $function = $this->soapV2ArrayMap[$method];
                $response = static::$clientv2->$function($this->sessionv2, $args);
                $this->noOfTries = 0;
                return $response;
            }
        } catch (SoapFault $fault) {

            if($fault->faultcode == 5 && $this->noOfTries < 3) {
                $this->getSession();
                $this->send($method, $args);
                $this->noOfTries++;
            } else {
                $this->noOfTries = 0;
            }

            $this->log(sprintf('SOAP Fault# Code:%s, Message:%s',$fault->faultcode, $fault->faultstring), Zend_Log::ERR);
        } catch (Exception $e) {
            $this->log($e->getMessage(), Zend_Log::ERR);
        }
        return false;
    }
}