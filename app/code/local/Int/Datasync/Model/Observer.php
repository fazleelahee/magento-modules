<?php

class Int_Datasync_Model_Observer
{

    public function editProduct($observer)
    {

    }


    public function addButtonTest($observer)
    {
        $container = $observer->getBlock();
        $link = Mage::helper('adminhtml')->getUrl('datasync/adminhtml_datasync/syncprocess/syncaction/1');
        if (null !== $container && $container->getType() == 'adminhtml/catalog_product') {
            $data = array(
                'label' => ' Sync products from PMT to Production',
                'class' => 'some-class',
                'onclick' => "setLocation('" . $link . "');$('loading-mask').show();",
            );
            $container->addButton('my_button_identifier', $data);
        }

        $datasyncCategoryEnabled = Mage::getStoreConfig('datasync/datasync_group/datasync_category');

        if($datasyncCategoryEnabled == "1") {
            $link = Mage::helper('adminhtml')->getUrl('datasync/adminhtml_datasync/categoryprocess');
            if (null !== $container && $container->getType() == 'adminhtml/catalog_product') {
                $data = array(
                    'label' => 'Sync Category from PMT to Production',
                    'class' => 'category-syc-class',
                    'onclick' => "setLocation('" . $link . "');$('loading-mask').show();",
                );
                $container->addButton('category_sync_btn', $data);
            }
            $link = Mage::helper('adminhtml')->getUrl('datasync/adminhtml_datasync/attributeprocess');

            if (null !== $container && $container->getType() == 'adminhtml/catalog_product') {
                $data = array(
                    'label' => 'Sync Attribute from PMT to Production',
                    'class' => 'attribute-syc-class',
                    'onclick' => "setLocation('" . $link . "');$('loading-mask').show();",
                );
                $container->addButton('attribute_sync_btn', $data);
            }
        }


        return $this;
    }

    public function addButtonNew($observer)
    {
        $container = $observer->getBlock();
        $link = Mage::helper('adminhtml')->getUrl('datasync/adminhtml_datasync/syncproductprocess');
        if (null !== $container && $container->getType() == 'adminhtml/catalog_product') {
            $data = array(
                'label' => 'Sync products from Production to PMT',
                'class' => 'some-classs',
                'onclick' => "setLocation('" . $link . "');$('loading-mask').show();",
            );
            $container->addButton('production_button', $data);
        }


        return $this;
    }





    public function onCategoryDeleteAfter($observer)
    {
        $datasyncCategoryEnabled = Mage::getStoreConfig('datasync/datasync_group/datasync_category');
        if($datasyncCategoryEnabled != "1") {
            return;
        }
        $category = $observer->getEvent()->getCategory();
        $soapConnection = Mage::getModel('datasync/productionsoap');
        try {
            //$soapConnection->getSession();
            $soapConnection->send('catalog_category.delete', $category->getId());
            $soapConnection->log("Category # {$category->getId()} has been deleted.", Zend_Log::INFO);
        } catch (Exception $e) {
            $soapConnection->log($e->getMessage(), Zend_Log::ERR);
        }
    }

    public function onAttributeDeleteAfter($observer)
    {
        $datasyncCategoryEnabled = Mage::getStoreConfig('datasync/datasync_group/datasync_category');
        if($datasyncCategoryEnabled != "1") {
            return;
        }
        $attribute = $observer->getEvent()->getAttibute();
        $soapConnection = Mage::getModel('datasync/productionsoap');
        try {
            //$soapConnection->getSession();
            $soapConnection->send("product_attribute.remove", [$attribute->getAttributeCode()]);
            $soapConnection->log("Attribute # {$attribute->getAttributeCode()} has been deleted.", Zend_Log::INFO);
        } catch (Exception $e) {
            $soapConnection->log($e->getMessage(), Zend_Log::ERR);
        }
    }

    public function onDefaultEntitySave($observer)
    {
        $datasyncCategoryEnabled = Mage::getStoreConfig('datasync/datasync_group/datasync_category');
        if($datasyncCategoryEnabled != "1") {
            return;
        }

        $object = @$observer->getEvent()->getDataObject()->getDataObject();
        $event = @$observer->getEvent()->getDataObject();


        try {
            if ($object instanceof Mage_Catalog_Model_Category) {
                $soapConnection = Mage::getModel('datasync/productionsoap');

                try {

                    $soapConnection->sendCategory($object);

                } catch (Exception $e) {
                    $soapConnection->log($e->getMessage(), Zend_Log::ERR);
                }
            } else if ($object instanceof Mage_Catalog_Model_Resource_Eav_Attribute) {

                $soapConnection = Mage::getModel('datasync/productionsoap');

                if ($event->getType() == "delete") {
                    try {
                        //$soapConnection->getSession();
                        $soapConnection->send("product_attribute.remove", [$object->getAttributeCode()]);
                        $soapConnection->log("Attribute # {$object->getAttributeCode()} has been deleted.", Zend_Log::INFO);
                    } catch (Exception $e) {
                        $soapConnection->log($e->getMessage(), Zend_Log::ERR);
                    }
                } else {

                    try {
                        //$soapConnection->getSession();
                        $soapConnection->sendAttribute($object);
                    } catch (Exception $e) {
                        $soapConnection->log($e->getMessage(), Zend_Log::ERR);
                    }
                }

            }
        } catch (Exception $e) {

        }

    }
}