<?php

/**
 * Class Int_Datasync_Model_ProductionSoap
 * Response for updating products/ category and attribute to destinations soap server.
 */
class Int_Datasync_Model_Productionsoap
{
    protected $dataSyncLogFile = 'sync-%s-%s-%s.log';
    protected static $client = null;
    protected $session = null;
    protected $noOfTries = 0;

    public function __construct()
    {

        $this->dataSyncLogFile = sprintf($this->dataSyncLogFile, date('Y'), date('m'), date('d'));
        $prodSoapUrl = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapiurl');
        try {
            if (static::$client == null) {
                static::$client = new SoapClient($prodSoapUrl);
            }
        } catch (SoapFault $fault) {
            $message = sprintf('SOAP Fault# Code:%s, Message:%s', $fault->faultcode, $fault->faultstring);
            $this->log($message, Zend_Log::ERR);
        }
        $this->getSession();
        return $this;
    }

    public function log($message, $status = Zend_log::DEBUG)
    {
        Mage::log($message, $status, $this->dataSyncLogFile, true);
    }

    public function getSession()
    {
        try {
            $prodSoapUsername = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapiusername');
            $prodSoapPassword = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapipass');

            if ($this->session == null) {
                $this->session = static::$client->login($prodSoapUsername, $prodSoapPassword);
            }
        } catch (SoapFault $fault) {
            $message = sprintf('SOAP Fault# Code:%s, Message:%s', $fault->faultcode, $fault->faultstring);
            $this->log($message, Zend_Log::ERR);
            throw new Exception ($message);
        }
        return $this;
    }

    /**
     *
     * @param $method
     * @param string $args
     * @return bool
     */

    public function send($method, $args = '')
    {
        try {
            if (is_array($args) || $args != '') {

                return static::$client->call($this->session, $method, $args);
            }

            return static::$client->call($this->session, $method);

        } catch (SoapFault $fault) {


            $this->log(sprintf('SOAP Fault# Code:%s, Message:%s', $fault->faultcode, $fault->faultstring), Zend_Log::ERR);
        } catch (Exception $e) {
            $this->log($e->getMessage(), Zend_Log::ERR);
        }
        return false;
    }

    /**
     * @param Mage_Catalog_Model_Category $category
     * @return array
     */
    public function prepareCategoryData(Mage_Catalog_Model_Category $category)
    {

        if ($category->getIsActive()) {
            $is_active = 1;
        } else {
            $is_active = 0;
        }

        $available_sort_by = $category->getAvailableSortBy();

        //$this->log($available_sort_by);
        if (count($available_sort_by) == 0) {
            $available_sort_by = ['Best Value', 'Name', 'Price', 'Lot'];
        }
        $default_sort_by = $category->getDefaultSortBy();
        //$this->log($default_sort_by);
        if (empty($default_sort_by) == 0) {
            $default_sort_by = 'Best Value';
        }

        $image = $category->getImage();
        Mage::log($image, Zend_Log::DEBUG, 'debug.log', true);
        $data = [
            'name' => $category->getName(),
            'is_active' => $is_active,
            'position' => $category->getPosition(),
            'available_sort_by' => $available_sort_by,
            'default_sort_by' => $default_sort_by,
            'description' => $category->getDescription(),
            'display_mode' => $category->getDisplayMode(),
            'is_anchor' => $category->getIsAnchor(),
            'landing_page' => $category->getLandingPage(),
            'meta_description' => $category->getMetaDescription(),
            'meta_keywords' => $category->getMetaKeywords(),
            'meta_title' => $category->getMetaTitle(),
            'page_layout' => $category->getPageLayout(),
            'url_key' => $category->getUrlKey(),
            'include_in_menu' => $category->getIncludeInMenu(),
            'filter_price_range' => $category->getFilterPriceRange(),
            'custom_use_parent_settings' => $category->getCustomUseParentSettings(),
            'image' => "http://ironehorse.local/media/catalog/category/download.png"
        ];

        return $data;
    }

    /**
     * @param Mage_Catalog_Model_Category $category
     * @param bool $update
     */
    public function sendCategory(Mage_Catalog_Model_Category $category, $update = true)
    {
        if (!is_object($category)) {
            $currentCatId = $category;
            $category = Mage::getModel('catalog/category')->load($category);
        } else {
            $currentCatId = $category->getId();
        }

        $info = $this->send('catalog_category.info', $currentCatId);
        $data = $this->prepareCategoryData($category);

        if ($info) {
            if (!$this->send('catalog_category.update', [$currentCatId, $data])) {
                Mage::log(sprintf('Unable to update category. PMT catId #%s', $currentCatId), Zend_Log::ERR, $this->dataSyncLogFile, true);
            } else {
                Mage::log(sprintf('Category updated. PMT catId #%s', $currentCatId), Zend_Log::INFO, $this->dataSyncLogFile, true);
            }
        } else {

            if (!$this->send('catalog_category.info', $category->getParentCategory()->getId())) {
                Mage::log(sprintf('Category ID# %s and parent category #%s not found.', $currentCatId, $category->getParentCategory()->getId()), Zend_Log::ERR, $this->dataSyncLogFile, true);
                return;
            }

            $id = $this->send('catalog_category.create', [$category->getParentCategory()->getId(), $data]);
            if (!$id) {
                Mage::log(sprintf('Unable to create a category. PMT catId #%s', $currentCatId), Zend_Log::ERR, $this->dataSyncLogFile, true);
            } else if ($id != $currentCatId) {
                Mage::log(sprintf('Category ID mis match . PMT catId #%s and Production catId#%s', $currentCatId, $id), Zend_Log::ERR, $this->dataSyncLogFile, true);
            } else {
                Mage::log(sprintf('Category created. PMT catId #%s and Production catId#%s', $currentCatId, $id), Zend_Log::INFO, $this->dataSyncLogFile, true);
            }
        }
    }


    /**
     * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
     * @return array
     */
    public function prepareAttributeData(Mage_Catalog_Model_Resource_Eav_Attribute $attribute)
    {
        $scope = $attribute->getScope();

        $data = [
            'attribute_code' => $attribute->getAttributeCode(),
            'frontend_input' => $attribute->getFrontendInput(),
            'scope' => $scope,
            'default_value' => $attribute->getDefaultValue(),
            'is_unique' => (int)$attribute->getIsUnique(),
            'is_required' => (int)$attribute->getIsRequired(),
            'is_configurable' => (int)$attribute->getIsConfigurable(),
            'is_searchable' => (int)$attribute->getIsSearchable(),
            'is_visible_in_advanced_search' => (int)$attribute->getIsVisibleInAdvancedSearch(),
            'is_comparable' => $attribute->getIsComparable(),
            'is_used_for_promo_rules' => $attribute->getIsUsedForPromoRules(),
            'is_visible_on_front' => $attribute->getIsVisibleOnFront(),
            'used_in_product_listing' => $attribute->getUsedInProductListing(),
        ];

        $labels = $attribute->getStoreLabels();


        if (count($labels) > 0) {
            foreach ($labels as $label) {
                if ($label != '') {
                    $data['frontend_label'][] = ['store_id' => 0, 'label' => $label];
                }

            }
        }

        if (!isset($data['frontend_label'])) {
            $label = $attribute->getFrontendLabel() != '' ? $attribute->getFrontendLabel() : $attribute->getAttributeCode();
            $data['frontend_label'][] = ['store_id' => 0, 'label' => $label];
        }


        $applyTo = $attribute->getApplyTo();
        if ($applyTo == null || count($applyTo) == 0) {
            $data['apply_to'] = ['simple'];
        }

        if ($attribute->getFrontendInput() == 'text') {
            $data['additional_fields'] = [
                'frontend_class' => $attribute->getFrontendClass(),
                'is_html_allowed_on_front' => boolval($attribute->getIsHtmlAllowedOnFront()),
                'used_for_sort_by' => boolval($attribute->getUsedForSortBy())
            ];
        }

        if ($attribute->getFrontendInput() == 'textarea') {
            $data['additional_fields'] = [
                'is_wysiwyg_enabled' => boolval($attribute->getIsWysiwygEnabled()),
                'is_html_allowed_on_front' => boolval($attribute->getIsHtmlAllowedOnFront())
            ];
        }

        if ($attribute->getFrontendInput() == 'date' || $attribute->getFrontendInput() == 'boolean') {
            $data['additional_fields'] = [
                'used_for_sort_by' => boolval($attribute->getUsedForSortBy())
            ];
        }

        if ($attribute->getFrontendInput() == 'multiselect') {
            $data['additional_fields'] = [
                'is_filterable' => boolval($attribute->getIsFilterable()),
                'is_filterable_in_search' => boolval($attribute->getIsFilterableInSearch()),
                'position' => (int)$attribute->getPosition()
            ];
        }

        if ($attribute->getFrontendInput() == 'select' || $attribute->getFrontendInput() == 'price') {
            $data['additional_fields'] = [
                'is_filterable' => boolval($attribute->getIsFilterable()),
                'is_filterable_in_search' => boolval($attribute->getIsFilterableInSearch()),
                'position' => (int)$attribute->getPosition(),
                'used_for_sort_by' => boolval($attribute->getUsedForSortBy())
            ];
        }

        return $data;
    }

    /**
     * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
     */
    public function sendAttribute(Mage_Catalog_Model_Resource_Eav_Attribute $attribute)
    {

        $data = $this->prepareAttributeData($attribute);
        $info = $this->send('product_attribute.info', $attribute->getAttributeId());

        if ($info) {
            if (!$this->send('product_attribute.update', ['id' => $attribute->getAttributeId(), 'data' => $data])) {
                $this->log(sprintf('Unable to update Product Attribute id #%s', $attribute->getAttributeId()), Zend_Log::ERR);
            } else {
                $this->log(sprintf('Product Attribute id #%s update has been updated.', $attribute->getAttributeId()), Zend_Log::INFO);
            }
        } else {

            //$id = $this->send('product_attribute.create', $data);
            $soapV2 = Mage::getModel('datasync/productionsoapv2');
            $id = $soapV2->send('product_attribute.create', $data);
            if (!$id) {
                $this->log(sprintf('PMT attribute ID#%s unable to create attribute in production.', $attribute->getAttributeId(), $id), Zend_Log::ERR);
            } else if ($attribute->getAttributeId() != $id) {
                $this->log(sprintf('PMT attribute ID#%s  & Production Attribute ID#%s does not match. ', $attribute->getAttributeId(), $id), Zend_Log::WARN);
            } else {
                $this->log(sprintf('PMT attribute ID#%s  & Production Attribute ID#%s created successfully.', $attribute->getAttributeId(), $id), Zend_Log::INFO);
            }
        }

    }

}