<?php

class Int_Datasync_Model_Mysql4_Datasync extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the datasync_id refers to the key field in your database table.
        $this->_init('datasync/datasync', 'datasync_id');
    }
}