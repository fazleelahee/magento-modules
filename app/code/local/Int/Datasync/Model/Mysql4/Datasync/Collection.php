<?php

class Int_Datasync_Model_Mysql4_Datasync_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('datasync/datasync');
    }
}