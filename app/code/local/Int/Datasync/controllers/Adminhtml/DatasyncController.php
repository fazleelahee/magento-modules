<?php

class Int_Datasync_Adminhtml_DatasyncController extends Mage_Adminhtml_Controller_action {

    protected $dataSyncLogFile = "dataSync.log";
    protected $isDebugging = false;
    protected $connectionData = array();
    protected $connectionType = false;

    const SYNC_PRODUCTION_TO_PMT = 1;
    const SYNC_PMT_TO_PRODUCTION = 2;

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('datasync/items')
                ->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));

        return $this;
    }

    public function indexAction() {
        $this->_initAction()
                ->renderLayout();
    }

    public function editAction() {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('datasync/datasync')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('datasync_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('datasync/items');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('datasync/adminhtml_datasync_edit'))
                    ->_addLeft($this->getLayout()->createBlock('datasync/adminhtml_datasync_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('datasync')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function saveAction() {
        if ($data = $this->getRequest()->getPost()) {

            if (isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
                try {
                    /* Starting upload */
                    $uploader = new Varien_File_Uploader('filename');

                    // Any extention would work
                    $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                    $uploader->setAllowRenameFiles(false);

                    // Set the file upload mode 
                    // false -> get the file directly in the specified folder
                    // true -> get the file in the product like folders 
                    //	(file.jpg will go in something like /media/f/i/file.jpg)
                    $uploader->setFilesDispersion(false);

                    // We set media as the upload dir
                    $path = Mage::getBaseDir('media') . DS;
                    $uploader->save($path, $_FILES['filename']['name']);
                } catch (Exception $e) {
                    
                }

                //this way the name is saved in DB
                $data['filename'] = $_FILES['filename']['name'];
            }


            $model = Mage::getModel('datasync/datasync');
            $model->setData($data)
                    ->setId($this->getRequest()->getParam('id'));

            try {
                if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
                    $model->setCreatedTime(now())
                            ->setUpdateTime(now());
                } else {
                    $model->setUpdateTime(now());
                }

                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('datasync')->__('Item was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('datasync')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('datasync/datasync');

                $model->setId($this->getRequest()->getParam('id'))
                        ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction() {
        $datasyncIds = $this->getRequest()->getParam('datasync');
        if (!is_array($datasyncIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($datasyncIds as $datasyncId) {
                    $datasync = Mage::getModel('datasync/datasync')->load($datasyncId);
                    $datasync->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('adminhtml')->__(
                                'Total of %d record(s) were successfully deleted', count($datasyncIds)
                        )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massStatusAction() {
        $datasyncIds = $this->getRequest()->getParam('datasync');
        if (!is_array($datasyncIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($datasyncIds as $datasyncId) {
                    $datasync = Mage::getSingleton('datasync/datasync')
                            ->load($datasyncId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) were successfully updated', count($datasyncIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function exportCsvAction() {
        $fileName = 'datasync.csv';
        $content = $this->getLayout()->createBlock('datasync/adminhtml_datasync_grid')
                ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction() {
        $fileName = 'datasync.xml';
        $content = $this->getLayout()->createBlock('datasync/adminhtml_datasync_grid')
                ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType = 'application/octet-stream') {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK', '');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }

    public function getSoapClientConnections($reConnect = false, $errMsg = '') {
        if ($errMsg != '') {
            Mage::log($errMsg, Zend_Log::ERR, $this->dataSyncLogFile, true);
        }
        if (count($this->connectionData) == 0 || $reConnect == true) {
            // Connection to PMT
            $pmtSoapUrl = Mage::getStoreConfig('datasync/datasync_group/datasync_firstapiurl');
            $pmtSoapUsername = Mage::getStoreConfig('datasync/datasync_group/datasync_firstapiusername');
            $pmtSoapPassword = Mage::getStoreConfig('datasync/datasync_group/datasync_firstapipass');

            $pmtClient = new SoapClient($pmtSoapUrl);
            $pmtSession = $pmtClient->login($pmtSoapUsername, $pmtSoapPassword);

            // Connection to Production
            $prodSoapUrl = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapiurl');
            $prodSoapUsername = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapiusername');
            $prodSoapPassword = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapipass');

            $prodClient = new SoapClient($prodSoapUrl);
            $prodSession = $prodClient->login($prodSoapUsername, $prodSoapPassword);
            if ($this->connectionType == self::SYNC_PRODUCTION_TO_PMT) {
                $this->connectionData = array(
                    'srcClient' => $prodClient,
                    'srcSessId' => $prodSession,
                    'destClient' => $pmtClient,
                    'destSessId' => $pmtSession,
                );
            } elseif ($this->connectionType == self::SYNC_PMT_TO_PRODUCTION) {
                $this->connectionData = array(
                    'destClient' => $prodClient,
                    'destSessId' => $prodSession,
                    'srcClient' => $pmtClient,
                    'srcSessId' => $pmtSession,
                );
            }
        }
        return $this->connectionData;
    }

    public function closeSoapConnections() {
        $conn = $this->getSoapClientConnections();
        if (isset($conn['srcClient']) && isset($conn['srcSessId'])) {
            try {
                $conn['srcClient']->endSession($conn['srcSessId']);
            } catch (Exception $e) {
                Mage::log("Closing Soap Connection: " . $e->getMessage(), Zend_Log::ERR, $this->dataSyncLogFile, true);
            }
        }
        if (isset($conn['destClient']) && isset($conn['destSessId'])) {
            try {
                $conn['destClient']->endSession($conn['destSessId']);
            } catch (Exception $e) {
                Mage::log("Closing Soap Connection: " . $e->getMessage(), Zend_Log::ERR, $this->dataSyncLogFile, true);
            }
        }
    }

    public function syncImages($productSKU) {
        $conn = $this->getSoapClientConnections();
        if ($this->isDebugging) {
            Mage::log("Start Image Sync Function", Zend_Log::DEBUG, $this->dataSyncLogFile, true);
        }

        //get a list of the existing images on PMT
        try {
            $oldImages = $conn['destClient']->call($conn['destSessId'], 'catalog_product_attribute_media.list', $productSKU);
        } catch (Exception $e) {
            $conn = $this->getSoapClientConnections(true, $e->getMessage());
            $oldImages = $conn['destClient']->call($conn['destSessId'], 'catalog_product_attribute_media.list', $productSKU);
        }
        foreach ($oldImages as $img) {
            if ($this->isDebugging) {
                Mage::log("Delete image: " . $img['file'], Zend_Log::DEBUG, $this->dataSyncLogFile, true);
            }

            //delete each image (they will be replaced by the images from PROD)
            try {
                $resultImageDelete = $conn['destClient']->call($conn['destSessId'], 'catalog_product_attribute_media.remove', array('product' => $productSKU, 'file' => $img['file']));
            } catch (Exception $e) {
                $conn = $this->getSoapClientConnections(true, $e->getMessage());
                $resultImageDelete = $conn['destClient']->call($conn['destSessId'], 'catalog_product_attribute_media.remove', array('product' => $productSKU, 'file' => $img['file']));
            }
        }

        //get a list of product images from PROD
        $product_images = $conn['srcClient']->call($conn['srcSessId'], 'catalog_product_attribute_media.list', $productSKU);
        for ($i = 0; $i < count($product_images); $i++) { //for each image
            //initialize product image filepath to empty
            unset($product_images[$i]['file']);

            //if image exists at specified location
            if (!exif_imagetype($product_images[$i]['url']) === false) {
                //load file from URL site
                $curl = curl_init($product_images[$i]['url']);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $ret_val = curl_exec($curl);

                if (!curl_errno($curl)) { //if image loads
                    $file = array(
                        'content' => chunk_split(base64_encode($ret_val)),
                        'mime' => curl_getinfo($curl, CURLINFO_CONTENT_TYPE),
                    );

                    //update product image object with file info
                    $product_images[$i]['file'] = $file;
                    if ($this->isDebugging) {
                        Mage::log("Create image", Zend_Log::DEBUG, $this->dataSyncLogFile, true);
                    }

                    //create the product image on PMT
                    try {
                        $resultImageCreation = $conn['destClient']->call($conn['destSessId'], "catalog_product_attribute_media.create", array($productSKU, $product_images[$i]));
                    } catch (Exception $e) {
                        $conn = $this->getSoapClientConnections(true, $e->getMessage());
                        $resultImageCreation = $conn['destClient']->call($conn['destSessId'], "catalog_product_attribute_media.create", array($productSKU, $product_images[$i]));
                    }
                }

                curl_close($curl);
            }
        }
    }

    public function createProduct($productSKU) {
        $conn = $this->getSoapClientConnections();

        //retrieve a list of product attribute sets from PMT
        try {
            $attributeSets = $conn['destClient']->call($conn['destSessId'], 'product_attribute_set.list');
            $attributeSet = current($attributeSets); //there is only one attribute set (Default), select it
        } catch (Exception $e) {
            $conn = $this->getSoapClientConnections(true, $e->getMessage());
            $attributeSets = $conn['destClient']->call($conn['destSessId'], 'product_attribute_set.list');
            $attributeSet = current($attributeSets); //there is only one attribute set (Default), select it
        }

        //load the product data from PROD (loading the default values)
        try {
            $product_details = $conn['srcClient']->call($conn['srcSessId'], 'product.info', array($productSKU, 0));
        } catch (Exception $e) {
            $conn = $this->getSoapClientConnections(true, $e->getMessage());
            $product_details = $conn['srcClient']->call($conn['srcSessId'], 'product.info', array($productSKU, 0));
        }
        //clear the sync setting so the product does not sync back from PMT
        $product_details['syncpro'] = 0;
        if (isset($product_details["tier_price"])) {
            unset($product_details["tier_price"]);
        }
        if (isset($product_details["group_price"])) {
            unset($product_details["group_price"]);
        }

        //create the product on PMT - this requires an attribute set_id and store_id
        try {
            $resultCreateProduct = $conn['destClient']->call($conn['destSessId'], 'catalog_product.create', array($product_details['type_id'], $attributeSet['set_id'], $productSKU, $product_details, 0));
        } catch (Exception $e) {
            $conn = $this->getSoapClientConnections(true, $e->getMessage());
            $resultCreateProduct = $conn['destClient']->call($conn['destSessId'], 'catalog_product.create', array($product_details['type_id'], $attributeSet['set_id'], $productSKU, $product_details, 0));
        }

        return $resultCreateProduct;
    }

    public function updateProductData($productSKU, $storeId) {
        $conn = $this->getSoapClientConnections();

        if ($this->isDebugging) {
            Mage::log("Syncing Store: " . $storeId, Zend_Log::DEBUG, $this->dataSyncLogFile, true);
        }
        //load the product data from PROD
        try {
            $product_details = $conn['srcClient']->call($conn['srcSessId'], 'product.info', array($productSKU, $storeId));
        } catch (Exception $e) {
            $conn = $this->getSoapClientConnections(true, $e->getMessage());
            $product_details = $conn['srcClient']->call($conn['srcSessId'], 'product.info', array($productSKU, $storeId));
        }

        //not clear why pricing info is being unset, I think it's updated separately
        unset($product_details['group_price']);
        unset($product_details['tier_price']);

        //clear the sync setting so the product does not sync back from PMT
        $product_details['syncpro'] = 0;

        //if product value is null (does not exist on source), use empty string so the attribute is updated with an empty value
        foreach ($product_details as $key => $value) {
            if (is_null($value)) {
                $product_details[$key] = "";
            }
        }

        //updates the PMT with the product details from the PROD
        try {
            $resultProductUpdate = $conn['destClient']->call($conn['destSessId'], 'catalog_product.update', array($productSKU, $product_details, $storeId));
        } catch (Exception $e) {
            $conn = $this->getSoapClientConnections(true, $e->getMessage());
            $resultProductUpdate = $conn['destClient']->call($conn['destSessId'], 'catalog_product.update', array($productSKU, $product_details, $storeId));
        }
        //TODO: verify this updated call actually updates all sections of the product (General, Meta Info, Search Filters, Recurring Profile?, Design, Gift Options, OldSite, Navision, Inventory, Product Video?, Websites, Categories, Related Products?, Up-sells?, Cross-sells?, Product Reviews, Product Tags?, Customers Tagged Product?, Custom Options?)
        //there is then code to handle the pricing information it was setup as a separate CURL call
        //This can be investigated separately
    }

    public function synchronizeAssociatedProducts($productSKU, $attributeSet, $deleteExisting = false) {
		$conn = $this->getSoapClientConnections();
		//get a list of associated products for the grouped product from the source
		try {
			$associatedProducts = $conn['srcClient']->call($conn['srcSessId'], 'catalog_product_link.list', array('type' => 'grouped', 'product' => $productSKU));
		} catch (Exception $e) {
			$conn = $this->getSoapClientConnections(true, $e->getMessage());
			$associatedProducts = $conn['srcClient']->call($conn['srcSessId'], 'catalog_product_link.list', array('type' => 'grouped', 'product' => $productSKU));
		}
		
		// Remove existing products association if the flag is true. true for existing product and false for new product.
		if($deleteExisting) {
			//get a list of the current assocation products from the destination
			try {
				$originalAssociatedProducts = $conn['destClient']->call($conn['destSessId'], 'catalog_product_link.list', array('type' => 'grouped', 'product' => $productSKU));
			} catch (Exception $e) {
				$conn = $this->getSoapClientConnections(true, $e->getMessage());
				$originalAssociatedProducts = $conn['destClient']->call($conn['destSessId'], 'catalog_product_link.list', array('type' => 'grouped', 'product' => $productSKU));
			}
	
			//go through each association to determine if it is still valid
			$calls = array();
			foreach ($originalAssociatedProducts as $originalAssociatedProduct) {
				//Remove the existing association
				$calls[$associatedProduct['sku']] = array('catalog_product_link.remove', array('type' => 'grouped', 'product' => $productSKU, 'linkedProduct' => $originalAssociatedProduct['sku']));
			}
			if(count($calls) > 0) {
				try {
					$conn['destClient']->multiCall($conn['destSessId'], $calls);
				} catch (Exception $e) {
					$conn = $this->getSoapClientConnections(true, $e->getMessage());
					$conn['destClient']->multiCall($conn['destSessId'], $calls);
				}
			}
		}
		
		if(count($associatedProducts) > 0) {
			//update the associations for grouped products
			$calls = array();
			foreach ($associatedProducts as $associatedProduct) {
				if ($this->isDebugging) {
					Mage::log("Associating the SKU " . $associatedProduct['sku'], Zend_Log::DEBUG, $this->dataSyncLogFile, true);
				}
				
				$childPro = array();
				try {
					$childPro = $conn['destClient']->call($conn['destSessId'], 'product.info', array($associatedProduct['sku'], 0));
				} catch (Exception $e) {
					$conn = $this->getSoapClientConnections(true, $e->getMessage());
					try {
						$childPro = $conn['destClient']->call($conn['destSessId'], 'product.info', array($associatedProduct['sku'], 0));
					} catch (Exception $e) {
						Mage::log("Check if Sku exists " . $e->getMessage(), Zend_Log::DEBUG, $this->dataSyncLogFile, true);
					}
				}
				if (!isset($childPro['product_id'])) {
					$childPro['product_id'] = $this->createProduct($associatedProduct['sku']);
				}

				$data = $associatedProduct;
				$data['product_id'] = $childPro['product_id'];
				$data['set'] = $attributeSet['set_id'];

				//associate simple products with the grouped product
				$calls[$associatedProduct['sku']] = array('catalog_product_link.assign', array('type' => 'grouped', 'sku' => $productSKU, 'linkedProduct' => $associatedProduct['sku'], 'data' => $data));
			}
			if(count($calls) > 0) {
				try {
					$conn['destClient']->multiCall($conn['destSessId'], $calls);
				} catch (Exception $e) {
					$conn = $this->getSoapClientConnections(true, $e->getMessage());
					$conn['destClient']->multiCall($conn['destSessId'], $calls);
				}
			}
		}
	}
	
    public function synchronizeProduct($productSKU, $productType, $activeStoreArr) {
        try {
            $conn = $this->getSoapClientConnections();

            if ($this->isDebugging) {
                Mage::log("Start Sync Product: " . $productSKU, Zend_Log::DEBUG, $this->dataSyncLogFile, true);
            }

            //retrieve a list of product attribute sets from PMT
            try {
                $attributeSets = $conn['destClient']->call($conn['destSessId'], 'product_attribute_set.list');
                $attributeSet = current($attributeSets); //there is only one attribute set (Default), select it
            } catch (Exception $e) {
                $conn = $this->getSoapClientConnections(true, $e->getMessage());
                try {
                    $attributeSets = $conn['destClient']->call($conn['destSessId'], 'product_attribute_set.list');
                    $attributeSet = current($attributeSets); //there is only one attribute set (Default), select it
                } catch (Exception $e) {
                    Mage::log("Get Attribute Set ID: " . $e->getMessage(), Zend_Log::DEBUG, $this->dataSyncLogFile, true);
                }
            }

            //attempt to load the product from the local environment PMT
            $id = null;
            try {
                $product_details = $conn['destClient']->call($conn['destSessId'], 'product.info', array($productSKU, 0));
                $id = $product_details['product_id'];
            } catch (Exception $e) {
                $conn = $this->getSoapClientConnections(true, $e->getMessage());
                try {
                    $product_details = $conn['destClient']->call($conn['destSessId'], 'product.info', array($productSKU, 0));
                    $id = $product_details['product_id'];
                } catch (Exception $e) {
                    Mage::log("Check If Product Exists: " . $e->getMessage(), Zend_Log::DEBUG, $this->dataSyncLogFile, true);
                }
            }

            //Mage::log($_product['sku'] . " images: " . var_dump($newImages), Zend_Log::DEBUG, $this->dataSyncLogFile, true);
            //If product Exists in local environment
            $productExistsLocally = $id != null;
            if ($productExistsLocally) {
                if ($this->isDebugging) {
                    Mage::log("Exists!", Zend_Log::DEBUG, $this->dataSyncLogFile, true);
                }

                foreach ($activeStoreArr as $storeId) {    //for each store
                    $this->updateProductData($productSKU, $storeId);

                    //Kiran - sync zonal prices from PROD to PMT //this needs to be reviewed for accuracy // Kiran - Verified
                    if ($this->connectionType == self::SYNC_PRODUCTION_TO_PMT) {
                        $this->syncZonalPricesToPMT($productSKU, $storeId, $id);
                    } elseif ($this->connectionType == self::SYNC_PMT_TO_PRODUCTION) {
                        $this->updateProductZonalPricesToProduction($productSKU, $storeId, $id);
                    }
                }

                //we need to update the grouped products associations if they have changed
                if ($productType == 'grouped') {
					$this->synchronizeAssociatedProducts($productSKU, $attributeSet, true);
				}
            } else {
                //product needs to be created
                if ($this->isDebugging) {
                    Mage::log("Does not exist!", Zend_Log::DEBUG, $this->dataSyncLogFile, true);
                }

                $id = $this->createProduct($productSKU);

                foreach ($activeStoreArr as $storeId) { //for each store
                    if ($storeId != 0) {
                        $this->updateProductData($productSKU, $storeId);
                    }
                    //Kiran - sync zonal prices from PROD to PMT //this needs to be reviewed for accuracy // Kiran - Verified
                    if ($this->connectionType == self::SYNC_PRODUCTION_TO_PMT) {
                        $this->syncZonalPricesToPMT($productSKU, $storeId, $id);
                    } elseif ($this->connectionType == self::SYNC_PMT_TO_PRODUCTION) {
                        $this->updateProductZonalPricesToProduction($productSKU, $storeId, $id);
                    }
                }

                //if the product is grouped we need to address the associated products
                if ($productType == 'grouped') {
					$this->synchronizeAssociatedProducts($productSKU, $attributeSet);
                }
            }

            if ($this->isDebugging) {
                Mage::log("Staring Images Sync", Zend_Log::DEBUG, $this->dataSyncLogFile, true);
            }
            //sync the images from PROD to PMT
            $this->syncImages($productSKU);

            if ($this->isDebugging) {
                Mage::log("Completed Images Sync", Zend_Log::DEBUG, $this->dataSyncLogFile, true);
                Mage::log("Start Marking as Imported", Zend_Log::DEBUG, $this->dataSyncLogFile, true);
            }
            //updates PROD to indicate sync completed
            try {
                $conn['srcClient']->call($conn['srcSessId'], 'catalog_product.update', array($productSKU, array('syncpro' => 0)));
            } catch (Exception $e) {
                $conn = $this->getSoapClientConnections(true, $e->getMessage());
                $conn['srcClient']->call($conn['srcSessId'], 'catalog_product.update', array($productSKU, array('syncpro' => 0)));
            }
            if ($this->isDebugging) {
                Mage::log("Product: " . $productSKU . " synced to PMT from PROD", Zend_Log::DEBUG, $this->dataSyncLogFile, true);
            }
            return true;
        } catch (Exception $e) {
            Mage::log($e->getMessage(), Zend_Log::ERR, $this->dataSyncLogFile, true);
            if ($this->isDebugging) {
                Mage::log($e, Zend_Log::DEBUG, $this->dataSyncLogFile, true);
            }
            return $e->getMessage();
        }
    }

    public function syncZonesList() {
        $conn = $this->getSoapClientConnections();
        if ($this->isDebugging) {
            Mage::log("Start syncZonesList", Zend_Log::DEBUG, $this->dataSyncLogFile, true);
        }

        try {
            $getZoneListOtherServer = $conn['destClient']->call($conn['destSessId'], 'northstar_zonalpricing.getZones');
        } catch (Exception $e) {
            $conn = $this->getSoapClientConnections(true, $e->getMessage());
            $getZoneListOtherServer = $conn['destClient']->call($conn['destSessId'], 'northstar_zonalpricing.getZones');
        }
        $otherZone = array();
        foreach ($getZoneListOtherServer as $otherServer) {
            $otherZone[] = $otherServer['zone_code'];
        }
        try {
            $getZoneList = $conn['srcClient']->call($conn['srcSessId'], 'northstar_zonalpricing.getZones');
        } catch (Exception $e) {
            $conn = $this->getSoapClientConnections(true, $e->getMessage());
            $getZoneList = $conn['srcClient']->call($conn['srcSessId'], 'northstar_zonalpricing.getZones');
        }
        foreach ($getZoneList as $zoneList) {
            if (!in_array($zoneList['zone_code'], $otherZone)) {
                try {
                    $conn['destClient']->call($conn['destSessId'], 'northstar_zonalpricing.setZones', array($zoneList));
                } catch (Exception $e) {
                    $conn = $this->getSoapClientConnections(true, $e->getMessage());
                    $conn['destClient']->call($conn['destSessId'], 'northstar_zonalpricing.setZones', array($zoneList));
                }
            }
        }
        if ($this->isDebugging) {
            Mage::log("Completed syncZonesList", Zend_Log::DEBUG, $this->dataSyncLogFile, true);
        }
    }

    public function syncZonalPricesToPMT($productSKU, $storeId, $id) {
        $conn = $this->getSoapClientConnections();
        try {
            $product_details = $conn['srcClient']->call($conn['srcSessId'], 'product.info', array($productSKU, $storeId));
        } catch (Exception $e) {
            $conn = $this->getSoapClientConnections(true, $e->getMessage());
            $product_details = $conn['srcClient']->call($conn['srcSessId'], 'product.info', array($productSKU, $storeId));
        }

        $curlUrlProduction = Mage::getStoreConfig('datasync/datasync_group/datasync_secondweburl');
        $productData = array();
        $pid = $product_details['product_id'];
        $zurl = $curlUrlProduction . '/zonepricesync/zonepricesync.php?productid=' . $pid . '&syncpid=' . $id . '&store_id=' . $storeId;
        $chs = curl_init();
        $timeout = 5;
        curl_setopt($chs, CURLOPT_URL, $zurl);
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($chs, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($chs, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($chs, CURLOPT_MAXREDIRS, 10);
        curl_setopt($chs, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($chs, CURLOPT_CONNECTTIMEOUT, $timeout);
        $zdata = curl_exec($chs);
        curl_close($chs);

        $productData = json_decode($zdata, true);
        if (!isset($productData['product_id'])) {
            $productData['product_id'] = $id;
        }
        $productData['store_id'] = $storeId;
        $productData['force_match'] = 1;  // To Match Zonal prices/Zonal Special Prices ...In case managento instance already had more zonal prices...Those will be deleted and keeps only latest ones.

        if (!empty($productData)) {
            try {
                $a = $conn['destClient']->call($conn['destSessId'], 'northstar_zonalpricing.addzonalprices', array($productData));
            } catch (Exception $e) {
                $conn = $this->getSoapClientConnections(true, $e->getMessage());
                $a = $conn['destClient']->call($conn['destSessId'], 'northstar_zonalpricing.addzonalprices', array($productData));
            }
        } else {
            $zproduct = Mage::getModel('catalog/product')->setStoreId($storeId)->load($id);
            $delzonePriceCollections = $zproduct->getZonalPrice();
            $delzoneSpecialPriceCollections = $zproduct->getZonalSpecialPrice();
            $delzoneGroupPriceCollection = $zproduct->getZonalGroupPrice();
            $delzoneTierPriceCollection = $zproduct->getZonalTierPrice();
            $delzonePriceFlag = 'no';
            $productData['product_id'] = $id;
            if (count($delzoneGroupPriceCollection) > 0) {
                $delzonePriceFlag = 'yes';
                foreach ($delzoneGroupPriceCollection as $delgroupPriceCollection) {
                    if ($delgroupPriceCollection['zone_code'] == 'default') {
                        $productData['group_price'][] = array(
                            'website_id' => $delgroupPriceCollection['website_id'],
                            'cust_group' => $delgroupPriceCollection['cust_group'],
                            'zone' => 0,
                            'price' => $delgroupPriceCollection['price'],
                            'delete' => 1
                        );
                    } else {
                        $pushArr = array('delete' => 1);
                        $delgroupPriceCollection = array_merge($delgroupPriceCollection, $pushArr);
                        $productData['group_price'][] = $delgroupPriceCollection;
                    }
                }
            }
            if (count($delzoneTierPriceCollection) > 0) {
                $delzonePriceFlag = 'yes';
                //omit checking another server has or not zone price setup, due to API not provide.
                foreach ($delzoneTierPriceCollection as $deltierPriceCollection) {
                    $tierType = $deltierPriceCollection['zone_code'];
                    if ($tierType == 'default') {
                        $productData['tier_price'][] = array(
                            'website_id' => $deltierPriceCollection['website_id'],
                            'cust_group' => $deltierPriceCollection['cust_group'],
                            'zone' => 0,
                            'price_qty' => $deltierPriceCollection['price_qty'],
                            'price' => $deltierPriceCollection['price'],
                            'delete' => 1
                        );
                    } else {
                        $pushArr = array('delete' => 1);
                        $deltierPriceCollection = array_merge($deltierPriceCollection, $pushArr);
                        $productData['tier_price'][] = $deltierPriceCollection;
                    }
                }
            }
            if (count($delzonePriceCollections) > 0) {
                $delzonePriceFlag = 'yes';
                //omit checking another server has or not zone price setup, due to API not provide.
                foreach ($delzonePriceCollections as $delzonePriceCollection) {
                    $productData['zonal_price'][] = array(
                        'zone_code' => $delzonePriceCollection['zone_code'],
                        'price' => $delzonePriceCollection['price'],
                        'delete' => 1
                    );
                }
            }
            if (count($delzoneSpecialPriceCollections) > 0) {
                $delzonePriceFlag = 'yes';
                //omit checking another server has or not zone price setup, due to API not provide.
                foreach ($delzoneSpecialPriceCollections as $delzoneSpecialPriceCollection) {
                    $productData['zonal_special_price'][] = array(
                        'zone_code' => $delzoneSpecialPriceCollection['zone_code'],
                        'special_price' => $delzoneSpecialPriceCollection['price'],
                        'delete' => 1
                    );
                }
            }
            if ($delzonePriceFlag == 'yes') {
                try {
                    $conn['destClient']->call($conn['destSessId'], 'northstar_zonalpricing.addzonalprices', array($productData));
                } catch (Exception $e) {
                    $conn = $this->getSoapClientConnections(true, $e->getMessage());
                    $conn['destClient']->call($conn['destSessId'], 'northstar_zonalpricing.addzonalprices', array($productData));
                }
            }
        }
    }

    //This is used when updating from PMT to PROD? Should be used with in function syncprocessAction()
    public function updateProductZonalPricesToProduction($productSKU, $storeId, $remoteProductId) {
        $conn = $this->getSoapClientConnections();
        $curlUrlProd = $curlUrlProd = Mage::getStoreConfig('datasync/datasync_group/datasync_secondweburl');
        $result = false;
        // Getting product model from local (PMT)
        $localProductId = Mage::getModel('catalog/product')->getResource()->getIdBySku($productSKU);
        $productModel = Mage::getModel('catalog/product')->setStoreId($storeId)->load($localProductId);
        // Load the product Zonal prices from product model
        $sku = $productModel->getSku();
        $zonePriceCollections = $productModel->getZonalPrice();
        $zoneSpecialPriceCollections = $productModel->getZonalSpecialPrice();
        $zoneGroupPriceCollection = $productModel->getZonalGroupPrice();
        $zoneTierPriceCollection = $productModel->getZonalTierPrice();

        // TODO: Following codes & logic needs to be verified
        $productData = array();
        if (count($zonePriceCollections) > 0 || count($zoneSpecialPriceCollections) > 0 || count($zoneGroupPriceCollection) > 0 || count($zoneTierPriceCollection) > 0) {
            $productData = array('product_id' => $remoteProductId);
            $productData['store_id'] = $storeId;
            $productData['force_match'] = 1; // To Match Zonal prices/Zonal Special Prices ...In case managento instance already had more zonal prices...Those will be deleted and keeps only latest ones.
        }
        if (count($zonePriceCollections) > 0) {
            foreach ($zonePriceCollections as $zonePriceCollection) {
                $productData['zonal_price'][] = array('zone_code' => $zonePriceCollection['zone_code'],
                    'price' => $zonePriceCollection['price']
                );
            }
        }
        if (count($zoneSpecialPriceCollections) > 0) {
            foreach ($zoneSpecialPriceCollections as $zoneSpecialPriceCollection) {
                $productData['zonal_special_price'][] = array('zone_code' => $zoneSpecialPriceCollection['zone_code'],
                    'special_price' => $zoneSpecialPriceCollection['price']
                );
            }
        }
        if (count($zoneGroupPriceCollection) > 0) {
            foreach ($zoneGroupPriceCollection as $groupPriceCollection) {
                if ($groupPriceCollection['zone_code'] == 'default') {
                    // TODO: verify whether it should be 'group_price' or 'zonal_group_price'
                    $productData['group_price'][] = array('website_id' => $groupPriceCollection['website_id'],
                        'cust_group' => $groupPriceCollection['cust_group'],
                        'zone' => 0, // TODO: verify whether it should be 'zone' or 'zone_id'
                        'price_qty' => $groupPriceCollection['price_qty'],
                        'price' => $groupPriceCollection['price']
                    );
                } else {
                    $productData['group_price'][] = $groupPriceCollection;
                }
            }
        }
        if (count($zoneTierPriceCollection) > 0) {
            foreach ($zoneTierPriceCollection as $tierPriceCollection) {
                if ($tierPriceCollection['zone_code'] == 'default') {
                    // TODO: verify whether it should be 'tier_price' or 'zonal_tier_price'
                    $productData['tier_price'][] = array('website_id' => $tierPriceCollection['website_id'],
                        'cust_group' => $tierPriceCollection['cust_group'],
                        'zone' => 0, // TODO: verify whether it should be 'zone' or 'zone_id'
                        'price_qty' => $tierPriceCollection['price_qty'],
                        'price' => $tierPriceCollection['price']
                    );
                } else {
                    $productData['tier_price'][] = $tierPriceCollection;
                }
            }
        }
        if (empty($productData)) {
            $zrmvurl = "{$curlUrlProd}/zonepricesync/zonepricedel.php?productid={$remoteProductId}&removezprice=yes&store_id={$storeId}"; // TODO: need to verify whether 'removezprice' is a spelling mistake or not
            $chz = curl_init();
            $timeout = 5;
            curl_setopt($chz, CURLOPT_URL, $zrmvurl);
            curl_setopt($chz, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($chz, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($chz, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($chz, CURLOPT_MAXREDIRS, 10);
            curl_setopt($chz, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($chz, CURLOPT_CONNECTTIMEOUT, $timeout);
            $zremvdata = curl_exec($chz);
            curl_close($chz);

            $productData = json_decode($zremvdata, true);
        }
        if (!empty($productData)) {
            if ($this->isDebugging) {
                Mage::log("Syncing (updating) product zonal prices for SKU: {$sku}", 7, $this->dataSyncLogFile, true);
            }
            // Updating product Zonal prices in Production with the product data from PMT
            try {
                $result = $conn['destClient']->call($conn['destSessId'], 'northstar_zonalpricing.addzonalprices', array($productData));
            } catch (Exception $e) {
                Mage::log($e->getMessage(), 3, $this->dataSyncLogFile, true);
            }
        }

        return $result;
    }

    public function categoryprocessAction() {
        $datasyncCategoryEnabled = Mage::getStoreConfig('datasync/datasync_group/datasync_category');
        if($datasyncCategoryEnabled != "1") {
            return;
        }

        $soapConnection = Mage::getModel('datasync/productionsoap');
        //$soapConnection->getSession();
        $categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('*');
        foreach($categories as $category) {
            try {
                $soapConnection->sendCategory($category);
            } catch (Exception $e) {
                $soapConnection->log($e->getMessage(), Zend_Log::ERR);
            }
        }
        $this->_redirect('adminhtml/catalog_product/index');
    }

    public function attributeprocessAction() {
        $datasyncCategoryEnabled = Mage::getStoreConfig('datasync/datasync_group/datasync_category');
        if($datasyncCategoryEnabled != "1") {
            return;
        }
        $soapConnection = Mage::getModel('datasync/productionsoap');
        //$attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'weight_type');
        try {
            //$soapConnection->getSession();
            $attributes = Mage::getResourceModel('catalog/product_attribute_collection')
                ->getItems();

            foreach ($attributes as $attribute){
                $soapConnection->sendAttribute($attribute);
            }
        } catch(Exception $e) {
            $soapConnection->log($e->getMessage(), Zend_Log::ERR);
        }
        $this->_redirect('adminhtml/catalog_product/index');
    }

    //Sync from PROD to PMT
    public function syncproductprocessAction() {

		$debugEnabled = Mage::getStoreConfig('datasync/datasync_group/datasync_debug');
		if($debugEnabled) {
			$this->isDebugging = true;
		}

        if ($this->isDebugging) {
            Mage::log("Sync Started from Production to PMT", Zend_Log::DEBUG, $this->dataSyncLogFile, true);
        }
        $this->connectionType = self::SYNC_PRODUCTION_TO_PMT;
        $conn = $this->getSoapClientConnections(true);

        //this needs to be reviewed for accuracy // Kiran - Done - To be discussed with james
        $this->syncZonesList();

        //get the list of stores from the PROD site
        try {
            $storelist = $conn['srcClient']->call($conn['srcSessId'], 'store.list');
        } catch (Exception $e) {
            $conn = $this->getSoapClientConnections(true, $e->getMessage());
            $storelist = $conn['srcClient']->call($conn['srcSessId'], 'store.list');
        }

        $productSyncArr = array();
        $activeStoreArr = array();
        $activeStoreArr[] = 0;

        foreach ($storelist as $storedetail) {
            $activeStore = $storedetail['is_active'];
            if ($activeStore) {
                $activeStoreArr[] = $storedetail['store_id'];
            }
        }
        if ($this->isDebugging) {
            Mage::log("Number stores: " . count($activeStoreArr), Zend_Log::DEBUG, $this->dataSyncLogFile, true);
        }
        //echo "Number stores: " . count($activeStoreArr);
        //get a list of products to SYNC from PROD
        $filters = array(array('syncpro' => array(1), 'blocked'=> 0));

        try {
            $products = $conn['srcClient']->call($conn['srcSessId'], 'product.list', $filters);
        } catch (Exception $e) {
            $conn = $this->getSoapClientConnections(true, $e->getMessage());
            $products = $conn['srcClient']->call($conn['srcSessId'], 'product.list', $filters);
        }

        if ($this->isDebugging) {
            Mage::log("Number products: " . count($products), Zend_Log::DEBUG, $this->dataSyncLogFile, true);
        }
        //echo "Number products: " . count($products);
        $syncStartTime = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        Mage::getSingleton('core/session')->setSyncStartTime($syncStartTime);
        if ($this->isDebugging) {
			Mage::log("Starting product syncing", 7, $this->dataSyncLogFile, true);
		}

        $syncLimit = Mage::getStoreConfig('datasync/datasync_group/datasync_maxlimit');
        $index = 0;
        $productSyncArr = array();
        foreach ($products as $_product) {
            $index++;
            $result = $this->synchronizeProduct($_product['sku'], $_product['type'], $activeStoreArr);
            if ($result === true) {
                $productSyncArr[] = $_product['sku'];
            }
            if ($index >= $syncLimit && $syncLimit > 0) {
                break;
            }
        }

        if (count($products) == 0) {
            $logDetails = "No product has been synced.";
            Mage::getSingleton('core/session')->addNotice('There is no product for Synchronization');
        } else {
            Mage::log("Products Synced successfully", Zend_Log::DEBUG, $this->dataSyncLogFile, true);
            $logDetails = implode(",", $productSyncArr);
            Mage::getSingleton('adminhtml/session')->addSuccess(count($productSyncArr) . " Product(s) synchronization succesfully completed");
        }

        $syncEndTime = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        $data = array('sync_from' => 'Production', 'sync_to' => 'PMT', 'log_details' => $logDetails, 'created_time' => $syncStartTime, 'finish_time' => $syncEndTime);
        $model = Mage::getModel('datasync/datasync')->setData($data);

        try {
            $model->save();
        } catch (Exception $e) {
            Mage::log($e->getMessage(), Zend_Log::ERR, $this->dataSyncLogFile, true);
        }

        // Close the Soap Connections
        $this->closeSoapConnections();
		
		// Sleep for 1 second and redirect to same page // Commented for now
		$redirectionEnabled = Mage::getStoreConfig('datasync/datasync_group/datasync_redirect');
		if ($index >= $syncLimit && $syncLimit > 0 && $redirectionEnabled) {
			sleep(1);
			$url = Mage::helper('adminhtml')->getUrl('datasync/adminhtml_datasync/syncproductprocess');
			Mage::app()->getFrontController()->getResponse()->setRedirect($url);
		} else {
			$this->_redirect('adminhtml/catalog_product/index');
		}
    }



    //sync from PMT to PROD? 
    public function syncprocessAction() {
		$debugEnabled = Mage::getStoreConfig('datasync/datasync_group/datasync_debug');
		if($debugEnabled) {
			$this->isDebugging = true;
		}
		
        if ($this->isDebugging) {
            Mage::log("Sync Started from PMT to Production", Zend_Log::DEBUG, $this->dataSyncLogFile, true);
        }
        $this->connectionType = self::SYNC_PMT_TO_PRODUCTION;
        $conn = $this->getSoapClientConnections(true);
		
        //this needs to be reviewed for accuracy
        $this->syncZonesList();

        //get the list of stores from the PROD site
        try {
            $storelist = $conn['srcClient']->call($conn['srcSessId'], 'store.list');
        } catch (Exception $e) {
            $conn = $this->getSoapClientConnections(true, $e->getMessage());
            $storelist = $conn['srcClient']->call($conn['srcSessId'], 'store.list');
        }

        $productSyncArr = array();
        $activeStoreArr = array();
        $activeStoreArr[] = 0;

        foreach ($storelist as $storedetail) {
            $activeStore = $storedetail['is_active'];
            if ($activeStore) {
                $activeStoreArr[] = $storedetail['store_id'];
            }
        }
        if ($this->isDebugging) {
            Mage::log("Number stores: " . count($activeStoreArr), Zend_Log::DEBUG, $this->dataSyncLogFile, true);
        }

//        //get a list of products to SYNC from PROD
//        $filters = array(array('syncpro' => array(1), 'blocked'=>array(0)));
//        try {
//            $products = $conn['srcClient']->call($conn['srcSessId'], 'product.list', $filters);
//        } catch (Exception $e) {
//            $conn = $this->getSoapClientConnections(true, $e->getMessage());
//            $products = $conn['srcClient']->call($conn['srcSessId'], 'product.list', $filters);
//        }

        $prodCollection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('type')
            ->addAttributeToSelect('type_id')
            ->addAttributeToSelect('syncpro')
            ->addFieldToFilter('syncpro', array(1))
            ->addFieldToFilter('blocked', 0)
            ->setOrder('updated_at', 'desc');
        $products = [];
        foreach($prodCollection as $product) {
            $products[] = array(
                'product_id'    => $product->getId(),
                'sku'           => $product->getSku(),
                'type'          => $product->getTypeId(),
                'set'           => $product->getAttributeSetId()
            );
        }

        if ($this->isDebugging) {
            Mage::log("Number products: " . count($products), Zend_Log::DEBUG, $this->dataSyncLogFile, true);
        }

        $syncStartTime = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        Mage::getSingleton('core/session')->setSyncStartTime($syncStartTime);
        if ($this->isDebugging) {
			Mage::log("Starting product syncing", 7, $this->dataSyncLogFile, true);
		}

        //for each product to sync
        $syncLimit = Mage::getStoreConfig('datasync/datasync_group/datasync_maxlimit');
        $index = 0;
        $productSyncArr = array();
        foreach ($products as $_product) {
            $index++;
            $result = $this->synchronizeProduct($_product['sku'], $_product['type'], $activeStoreArr);
            if ($result === true) {
                $productSyncArr[] = $_product['sku'];
            }
            if ($index >= $syncLimit && $syncLimit > 0) {
                break;
            }
        }

        // Close the Soap Connections
        //return to the index page

        if (count($products) == 0) {
            $logDetails = "No product has been synced.";
            Mage::getSingleton('core/session')->addNotice('There is no product for Synchronization');
        } else {
            Mage::log("Product Synced successfully", Zend_Log::DEBUG, $this->dataSyncLogFile, true);
            $logDetails = implode(",", $productSyncArr);
            Mage::getSingleton('adminhtml/session')->addSuccess(count($productSyncArr) . " Product(s) synchronization succesfully completed");
        }

        $syncEndTime = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        $data = array('sync_from' => 'PMT', 'sync_to' => 'Production', 'log_details' => $logDetails, 'created_time' => $syncStartTime, 'finish_time' => $syncEndTime);
        $model = Mage::getModel('datasync/datasync')->setData($data);
        try {
            $model->save();
        } catch (Exception $e) {
            Mage::log($e->getMessage(), Zend_Log::ERR, $this->dataSyncLogFile, true);
        }
        $this->closeSoapConnections();
		
		// Sleep for 1 second and redirect to same page // Commented for now
		$redirectionEnabled = Mage::getStoreConfig('datasync/datasync_group/datasync_redirect');
		if ($index >= $syncLimit && $syncLimit > 0 && $redirectionEnabled) {
			sleep(1);
			$url = Mage::helper('adminhtml')->getUrl('datasync/adminhtml_datasync/syncprocess/syncaction/1');
			Mage::app()->getFrontController()->getResponse()->setRedirect($url);
		} else {
			$this->_redirect('adminhtml/catalog_product/index');
		}
    }

}