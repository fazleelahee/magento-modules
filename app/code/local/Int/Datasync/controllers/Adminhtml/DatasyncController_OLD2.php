<?php

class Int_Datasync_Adminhtml_DatasyncController extends Mage_Adminhtml_Controller_action
{
	protected $dataSyncLogFile = "dataSync.log";
	
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('datasync/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('datasync/datasync')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('datasync_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('datasync/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('datasync/adminhtml_datasync_edit'))
				->_addLeft($this->getLayout()->createBlock('datasync/adminhtml_datasync_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('datasync')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
			
			if(isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
				try {	
					/* Starting upload */	
					$uploader = new Varien_File_Uploader('filename');
					
					// Any extention would work
	           		$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
					$uploader->setAllowRenameFiles(false);
					
					// Set the file upload mode 
					// false -> get the file directly in the specified folder
					// true -> get the file in the product like folders 
					//	(file.jpg will go in something like /media/f/i/file.jpg)
					$uploader->setFilesDispersion(false);
							
					// We set media as the upload dir
					$path = Mage::getBaseDir('media') . DS ;
					$uploader->save($path, $_FILES['filename']['name'] );
					
				} catch (Exception $e) {
		      
		        }
	        
		        //this way the name is saved in DB
	  			$data['filename'] = $_FILES['filename']['name'];
			}
	  			
	  			
			$model = Mage::getModel('datasync/datasync');		
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			
			try {
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
						->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}	
				
				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('datasync')->__('Item was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('datasync')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('datasync/datasync');
				 
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
					 
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
        $datasyncIds = $this->getRequest()->getParam('datasync');
        if(!is_array($datasyncIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($datasyncIds as $datasyncId) {
                    $datasync = Mage::getModel('datasync/datasync')->load($datasyncId);
                    $datasync->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($datasyncIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
    public function massStatusAction()
    {
        $datasyncIds = $this->getRequest()->getParam('datasync');
        if(!is_array($datasyncIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($datasyncIds as $datasyncId) {
                    $datasync = Mage::getSingleton('datasync/datasync')
                        ->load($datasyncId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($datasyncIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
  
    public function exportCsvAction()
    {
        $fileName   = 'datasync.csv';
        $content    = $this->getLayout()->createBlock('datasync/adminhtml_datasync_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'datasync.xml';
        $content    = $this->getLayout()->createBlock('datasync/adminhtml_datasync_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
    
    
    
       public function syncprocessAction()
       {
       		//$clientiron = new SoapClient('http://leftshark.northstarhosting.com/index.php/api/soap/?wsdl');
			//$sessionId = $clientiron->login('ironuser', 'ironapikey');

			$clientironurl = Mage::getStoreConfig('datasync/datasync_group/datasync_firstapiurl');

			$ironusername = Mage::getStoreConfig('datasync/datasync_group/datasync_firstapiusername');
			$ironpass = Mage::getStoreConfig('datasync/datasync_group/datasync_firstapipass');

			$clientiron = new SoapClient($clientironurl);
			$sessionId = $clientiron->login($ironusername, $ironpass);

       		//$client = new SoapClient('http://proudsheep.northstarhosting.com/index.php/api/soap/?wsdl');
			//$session = $client->login('thunderuser', 'thunderapikey');


			$clienturl = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapiurl');

			$clientusername = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapiusername');
			$clientpass = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapipass');

			$client = new SoapClient($clienturl);
			$session = $client->login($clientusername, $clientpass);
			
			
			$curlUrlProduction = Mage::getStoreConfig('datasync/datasync_group/datasync_secondweburl');


			$this->createZoneListToProduction();



       		// $zoneCollections = Mage::getModel('northstar_zonalpricing/zonalwiseprices')->getCollection(); 

	       	// echo "<pre>";
	       	// print_r($zoneCollections->getData());
	       	// die();


       		$syncLimit = Mage::getStoreConfig('datasync/datasync_group/datasync_maxlimit');

       		$storelist = $clientiron->call($sessionId, 'store.list');
	
			$productSyncArr = array();
			$activeStoreArr = array();
			$activeStoreArr[] = 0;
			foreach($storelist as $storedetail){		
				$activeStore = $storedetail['is_active'];
				if($activeStore){			
					$activeStoreArr[] = $storedetail['store_id'];
				}
			}
                				
			$mediapath=Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
			$syncaction=$this->getRequest()->getparam('syncaction');
			Mage::register('syncation', $syncaction);
			
			/*
			$allStores = Mage::app()->getStores();
			$allStoreArr = array();
			$allStoreArr[] = 0;

			foreach ($allStores as $_eachStoreId)
			{
				$_storeId = Mage::app()->getStore($_eachStoreId)->getId();
				$allStoreArr[] = $_storeId;
			}

			//print_r($allStores);
			//exit;

			*/

			$totProduct = 0;
			
			$i=0;
			$j= 0;
		
			$filters = array(array('syncpro' => array(1)));			
			$products = $clientiron->call($sessionId, 'product.list', $filters);

			$syncPageLimit = ceil((count($products) / $syncLimit));

			// if(count($products) > 0 && $syncLimit > count($products))
			// {
			// 	$syncPageLimit = 1;
			// }
			
			/*echo $syncLimit.'<br>';
			echo $syncPageLimit.'<br>';
			echo count($products).'<br>';
			exit;*/

			// for($j = 0; $j < $syncPageLimit; $j++)
			// {
			// 	echo "loop</br>";

			// }

			// die();

			if(count($products)>0){

				$syncStartTime = Mage::getModel('core/date')->date('Y-m-d H:i:s');

				Mage::getSingleton('core/session')->setSyncStartTime($syncStartTime);
			
				for($j = 0; $j < $syncPageLimit; $j++)
			 	{
				
					foreach ($activeStoreArr as $_storeId)
					{
			
						//$_storeId = Mage::app()->getStore($_eachStoreId)->getId();
			
						//$collection=Mage::getModel('catalog/product')->getCollection()->addAttributeToFilter('syncpro',1);


				
						foreach($products as $product){

							if (!in_array($product['sku'], $productSyncArr))
							{
					  			$productSyncArr[] = $product['sku'];

							}
					
							if($product['sku']!=""){
					
								$id = Mage::getModel('catalog/product')->getResource()->getIdBySku($product['sku']);


								//exit;
					
								$_product=Mage::getModel('catalog/product')->setStoreId($_storeId)->load($id);


								//echo "<pre>";
								//echo $_product->getTypeId();
								//print_r($_product->getData());
								//die();


					
								$categoryIds = $_product->getCategoryIds();
								$websiteids=$_product->getWebsiteIds() ;
								// $catidarr['categories']=$categoryIds;
								// $catidarr['websites']=$websiteids;
				
					
					
								$data=$_product->getData();

								$stockitem=$data['stock_item'];
								//$stockdata=$stockitem->getData();
								$newImages=$data['media_gallery']['images'];		
								unset($data['stock_item']);
								$mediapath=Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
								//$data = array_merge($data, $catidarr);
				
								// print_r( $data);
								foreach ($data as $k => $v) {
						
									if($k == 'category_ids'){
										$cv=$v;
										unset ($data[$k]);
										$new_key = 'categories';
										
										$data[$new_key] = $cv;
									}
										
									if($k == 'website_ids'){
										$cv=$v;
										unset ($data[$k]);
										$new_key = 'websites';
										
										$data[$new_key] = $cv;
									}
										
									if($k == 'syncpro'){
										$data['syncpro'] = 0;
									}
								}
				

				 				//  $storeinfo = $client->call($session, 'store.info', $_storeId);
				  
								$product_exist=null;
								try{
									$product_exist=$client->call($session, 'product.info', array($_product->getSku()));
					
								}
								catch(exception $e){
								//nothing to do
									Mage::log($e->getMessage(), 7, $this->dataSyncLogFile, true);
								}
				
				
								if($product_exist==null or  $product_exist==""){
									$attributeSets = $client->call($session, 'product_attribute_set.list');
									$attributeSet = current($attributeSets);		
									unset($data['group_price']);
									unset($data['tier_price']);    
									$result = $client->call($session, 'catalog_product.create', array($_product->getTypeID(), $attributeSet['set_id'], $_product->getSku(), $data, $_storeId));
									//$client->call($session,'product_stock.update',array($_product->getSku(),$stockdata));
									

									/* Code for Zone Price - added on - 27.08.2015 */


									//$zoneCollections = Mage::getModel('northstar_zonalpricing/zonalwiseprices')->getCollection(); 





									/* Code for Grouped Product */ 
						
									if($_product->getTypeId() == 'grouped')
									{
										$associateSku = array();
										$newProductSku = array();

										$associatedProduct = $_product->getTypeInstance(true)->getAssociatedProducts($_product);

										
										foreach($associatedProduct as $associatedPro)
										{	
											
											$groupProductExist = null;
											//$groupProductNotExist = null;
									
											try{
													$groupProductExist = $client->call($session, 'product.info', array($associatedPro['sku']));

											}
											catch(exception $e){
												//nothing to do
												Mage::log($e->getMessage(), 7, $this->dataSyncLogFile, true);
											}


											 if(!$groupProductExist==null || !$groupProductExist=="")
											 {
											 	$associateSku[] =  $associatedPro['sku'];
											 }else{
											 	$newProductSku[] =  $associatedPro['sku'];
											 }

										}

										if(!empty($newProductSku))
										{
											
											foreach($newProductSku as $simpleProdSku)
											{
												foreach ($activeStoreArr as $storeId)
												{

															$id = Mage::getModel('catalog/product')->getResource()->getIdBySku($simpleProdSku);
							
															$groupSimpleProduct=Mage::getModel('catalog/product')->setStoreId($storeId)->load($id);


															$categoryIds = $groupSimpleProduct->getCategoryIds();
															$websiteids=$groupSimpleProduct->getWebsiteIds() ;

															$data=$groupSimpleProduct->getData();

															$stockitem=$data['stock_item'];
							
															$newImages=$data['media_gallery']['images'];		
															unset($data['stock_item']);
															$mediapath=Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
							
															foreach ($data as $k => $v) {
													
																if($k == 'category_ids'){
																	$cv=$v;
																	unset ($data[$k]);
																	$new_key = 'categories';
																	
																	$data[$new_key] = $cv;
																}
																	
																if($k == 'website_ids'){
																	$cv=$v;
																	unset ($data[$k]);
																	$new_key = 'websites';
																	
																	$data[$new_key] = $cv;
																}
																	
																if($k == 'syncpro'){
																	$data['syncpro'] = 0;
																}
															}
														

													$product_exist=null;
													
													try{
															$product_exist=$client->call($session, 'product.info', array($simpleProdSku));
					
													}
													catch(exception $e){
														//nothing to do
														Mage::log($e->getMessage(), 7, $this->dataSyncLogFile, true);
													}

														if($product_exist==null or  $product_exist=="")
														{

															$attributeSets = $client->call($session, 'product_attribute_set.list');
															$attributeSet = current($attributeSets);		    
															$client->call($session, 'catalog_product.create', array($groupSimpleProduct->getTypeID(), $attributeSet['set_id'], $groupSimpleProduct->getSku(), $data, $storeId));

															//$associateSku[] =  $simpleProdSku;
															//unset($newProductSku[$simpleProdSku]);
															
														}else
														{
															$client->call($session, 'catalog_product.update',array($groupSimpleProduct->getSku(), $data, $storeId));
															
															
															$oldimg= $client->call($session, 'catalog_product_attribute_media.list', $groupSimpleProduct->getSku());
				      				

				      										if($oldimg > 0)
				      										{
							
																foreach($oldimg as $img){
												
																	try{
																	$resultr=$client->call($session, 'catalog_product_attribute_media.remove', array('product'=>$groupSimpleProduct->getSku(), 'file'=>$img['file']));
																	//$client->catalogProductAttributeMediaRemove((object)array('sessionId' => $session->result, 'productId' =>$_product->getSku(), 'file' => $img['file']));
																
																  
																	}catch(exception $e){
																		Mage::log($e->getMessage(), 7, $this->dataSyncLogFile, true);
																	}
					
				       											}
				       										}
					 

															$product_images = $clientiron->call($sessionId, 'catalog_product_attribute_media.list', $groupSimpleProduct->getSku());

								
																	if(count($product_images) > 0)
																	{
								
																		for($i=0;$i < count($product_images) ;$i++){
																			unset($product_images[$i]['file']);
																			$curl = curl_init($product_images[$i]['url']);
								
																			if (!exif_imagetype($product_images[$i]['url']) === false)
																			{
																				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true );
																				$ret_val = curl_exec($curl);
																					if(!curl_errno($curl)){
																						$file = array(
																							'content' => chunk_split(base64_encode($ret_val)),
																							'mime' => curl_getinfo($curl , CURLINFO_CONTENT_TYPE),
																						);
								
																						$product_images[$i]['file']=$file;
																						$client->call($session,"catalog_product_attribute_media.create", array($groupSimpleProduct->getSku(), $product_images[$i]));
								
																					}
																				curl_close($curl);
																			}
																		}
																	}

														}
												}

												//$product = Mage::getModel('catalog/product')->loadByAttribute('sku',$simpleProdSku);

												//$simpleAttributeSets = $client->call($session, 'product_attribute_set.list');
												//$simplettributeSet = current($simpleAttributeSets);		    
												//$result = $client->call($session, 'catalog_product.create', array($_product->getTypeID(), $simplettributeSet['set_id'], $simpleProdSku, $data, $_storeId));
											
												if (!in_array($simpleProdSku, $associateSku))
												{
					  								$associateSku[] =  $simpleProdSku;

												}

											}
											//	print_r($result);
												//die();

										}

										// for($i=0; $i < count($associatedProduct); $i++)
										// {
										// 	echo $associatedProduct['sku'];
										// }


										//echo "<pre>";
										//print_r($associatedProduct->getSku());
					

										if(count($associateSku > 0))
										{
											//$commaSepSku = implode(",",$associateSku);
											//$commaSepSku = trim($commaSepSku);

											foreach($associateSku as $assoSku)
											{
												$client->call($session, 'catalog_product_link.assign', array('type' => 'grouped', 'sku' => $_product->getSku(), 'linkedProduct' => $assoSku));
											}

										}

									}



									/*
									foreach($newImages as $img){
						
										$mpath= $mediapath."catalog/product/".$img['file'];
										$flnm=end(explode("/" , $img['file'] ));
										$fnm=explode("." , $flnm);
											
										$file = array(
											'name' => $fnm[0], 
											'content' => base64_encode(file_get_contents($mpath)),
											'mime' => 'image/'.$fnm[1]
											);
									
										try{
										//$client->call($session, 'catalog_product_attribute_media.remove', array('product' =>$_product->getSku(), 'file'=>$img['file']));
										$client->call($session, 'catalog_product_attribute_media.create',array( $_product->getSku(),array('file'=>$file, 'label' => $img['label'], 'position' => $img['position'], 'types' =>$img['types'], 'exclude' => $img['disabled']),$_storeId));
										
										}catch(exception $e){
										}
						
									}
										*/
					
						
						
									if($result)
									{
										$_product->setData('syncpro', 0)->save();
									
										//$clientiron->call($sessionId, 'catalog_product.update',array($_product->getSku(), array('syncpro'=>0)));			
										//Mage::getSingleton('adminhtml/session')->addSuccess('Synchronization succesfully completed');
										//$this->_redirect('adminhtml/catalog_product/index');
									
									}else{		
										Mage::getSingleton('adminhtml/session')->addError('Synchronization unsuccesfull');
										$this->_redirect('adminhtml/catalog_product/index');
									}
					
					
								}else {

									unset($data['group_price']);
									unset($data['tier_price']);

									$result = $client->call($session, 'catalog_product.update',array($_product->getSku(), $data, $_storeId));
					
									//$client->call($session,'product_stock.update',array($_product->getSku(),$stockdata));

									/* code for Zone Price */






									    $productss = Mage::getModel('catalog/product')->load($id);


										$dtierPrice = $productss->getTierPrice();

									   	$zonePriceCollections =  $productss->getZonalPrice();
									    $zoneSpecialPriceCollections = $productss->getZonalSpecialPrice();
									    $zoneGroupPriceCollection = $productss->getZonalGroupPrice();
									    $zoneTierPriceCollection = $productss->getZonalTierPrice();



									// $zonePriceCollections = Mage::getModel('northstar_zonalpricing/zonalwiseprices')->getCollection()
									// 																				->addFieldToFilter('product_id', $id);

									// $zoneGroupPriceCollection = Mage::getModel('northstar_zonalpricing/zonalgrouppricing')->getCollection()
									//  																					 	->addFieldToFilter('entity_id', $id);

									// $zoneTierPriceCollection = Mage::getModel('northstar_zonalpricing/zonaltierpricing')->getCollection()
									//  																					 ->addFieldToFilter('entity_id', $id);


									//$zoneCollections = Mage::getModel('northstar_zonalpricing/zonalpricing')->getCollection();
									// echo "<pre>";
									// print_r($zoneTierPriceCollection->getData());
									// print_r($zoneGroupPriceCollection->getData());
									// die();																			

									$product_exist=null;

									try{
										$product_exist=$client->call($session, 'product.info', array($_product->getSku()));
						
									}
									catch(exception $e){
									//nothing to do
										Mage::log($e->getMessage(), 7, $this->dataSyncLogFile, true);
									}

									$getOtherServerProdId = $product_exist['product_id'];
									$productData = array();


									if(count($zonePriceCollections) > 0 || count($zoneSpecialPriceCollections) > 0 || count($zoneGroupPriceCollection) > 0 || count($zoneTierPriceCollection) > 0 )
									{
										$productData = array('product_id' => $getOtherServerProdId);
										
									}


									$zonePriceFlag = 'no';

									/*if($_storeId == 0)
									{

									if(count($zonePriceCollections) > 0)
									{
										$zonePriceFlag = 'yes';
										//omit checking another server has or not zone price setup, due to API not provide.

										foreach($zonePriceCollections as $priceCollection)
										{
											if($priceCollection['is_special_price'] == 1)
											{
												$productData['zonal_special_price'] = array(

																					array( 
																						'zone_id' => $priceCollection['zone_id'],
																						'special_price' => $priceCollection['price']

																						)
																			);

											}else if($priceCollection['is_special_price'] == 0)
											{
												$productData['zonal_price'] = array(

																					array( 
																						'zone_id' => $priceCollection['zone_id'],
																						'special_price' => $priceCollection['price']

																						)
																			);

											}


										}

									}


									if(count($zoneGroupPriceCollection) > 0)
									{
										$zonePriceFlag = 'yes';
										//omit checking another server has or not zone price setup, due to API not provide.
										$productData['group_price'] = array();

										foreach($zoneGroupPriceCollection as $groupPriceCollection)
										{
											
											$productData['group_price'][] =  array(
																					'website_id' => $groupPriceCollection['website_id'],
																					'cust_group' => $groupPriceCollection['customer_group_id'],
																					'zone' => $groupPriceCollection['zone_id'],
																					'price' => $groupPriceCollection['value']

																					);


										}

									}


									if(count($zoneTierPriceCollection) > 0)
									{
										$zonePriceFlag = 'yes';
										//omit checking another server has or not zone price setup, due to API not provide.

										foreach($zoneTierPriceCollection as $tierPriceCollection)
										{
											
											$productData['tier_price'][] = array(
																					'website_id' => $tierPriceCollection['website_id'],
																					'cust_group' => $tierPriceCollection['customer_group_id'],
																					'zone' => $tierPriceCollection['zone_id'],
																					'price_qty' => $tierPriceCollection['qty'],
																					'price' => $tierPriceCollection['value'],

																					);

										}

									}	


									echo "<pre>";
									print_r($productData);
									die();
									
										if($zonePriceFlag == 'yes')
										{
											$result = $client->call($session, 'northstar_zonalpricing.addzonalprices', array($productData));
										}	
									} */																		
								
									if(count($zoneGroupPriceCollection) > 0)
									{
										$zonePriceFlag = 'yes';
										//omit checking another server has or not zone price setup, due to API not provide.
										//$productData['group_price'] = array();

										foreach($zoneGroupPriceCollection as $groupPriceCollection)
										{
											if($groupPriceCollection['zone_code'] == 'default')
											{
												$productData['group_price'][] =  array(
																						'website_id' => $groupPriceCollection['website_id'],
																						'cust_group' => $groupPriceCollection['cust_group'],
																						'zone' => 0,
																						'price_qty' => $groupPriceCollection['price_qty'],
																						'price' => $groupPriceCollection['price']

																						);


											}else{
												$productData['group_price'][] =  $groupPriceCollection;
											}
											
											


										}

									}



									if(count($zoneTierPriceCollection) > 0)
									{
										$zonePriceFlag = 'yes';
										//omit checking another server has or not zone price setup, due to API not provide.

										foreach($zoneTierPriceCollection as $tierPriceCollection)
										{
											$tierType = $tierPriceCollection['zone_code'];
											if($tierType == 'default')
											{
												$productData['tier_price'][] = array(
																					'website_id' => $tierPriceCollection['website_id'],
																					'cust_group' => $tierPriceCollection['cust_group'],
																					'zone' => 0,
																					'price_qty' => $tierPriceCollection['price_qty'],
																					'price' => $tierPriceCollection['price']

																					);


											}else{
												$productData['tier_price'][] = $tierPriceCollection;
											}
											
											
										}

									}


									// if(count($dtierPrice) > 0)
									// {
									// 	$zonePriceFlag = 'yes';
									// 	//omit checking another server has or not zone price setup, due to API not provide.
									// 	//$productData['zonal_price'] = array();

									// 	foreach($dtierPrice as $defTierPrice)
									// 	{
											
									// 		$productData['tier_price'][] = array(
									// 												'website_id' => $defTierPrice['website_id'],
									// 												'cust_group' => $defTierPrice['cust_group'],
									// 												'zone' => 0,
									// 												'price_qty' => $defTierPrice['price_qty'],
									// 												'price' => $defTierPrice['price']

									// 												);
																		

									// 	}

									// }


									if(count($zonePriceCollections) > 0)
									{
										$zonePriceFlag = 'yes';
										//omit checking another server has or not zone price setup, due to API not provide.
										//$productData['zonal_price'] = array();

										foreach($zonePriceCollections as $zonePriceCollection)
										{
											
											$productData['zonal_price'][] = 
																					array( 
																						'zone_code' => $zonePriceCollection['zone_code'],
																						'price' => $zonePriceCollection['price']

																						);
																			


										}

									}

									if(count($zoneSpecialPriceCollections) > 0)
									{
										$zonePriceFlag = 'yes';
										//omit checking another server has or not zone price setup, due to API not provide.
										//$productData['group_price'] = array();

										foreach($zoneSpecialPriceCollections as $zoneSpecialPriceCollection)
										{
											
											$productData['zonal_special_price'][] =

																					array( 
																						'zone_code' => $zoneSpecialPriceCollection['zone_code'],
																						'special_price' => $zoneSpecialPriceCollection['price']

																						);

										}

									}


									if(!empty($productData))
									{
										$result = $client->call($session, 'northstar_zonalpricing.addzonalprices', array($productData));
									}else{

										$zrmvurl = $curlUrlProduction.'/zonepricesync/zonepricedel.php?productid='.$getOtherServerProdId.'&removezprice=yes';
										$chz = curl_init();
										$timeout = 5;
										curl_setopt($chz, CURLOPT_URL, $zrmvurl);
										curl_setopt($chz, CURLOPT_RETURNTRANSFER, 1);
										curl_setopt($chz, CURLOPT_SSL_VERIFYHOST,false);
										curl_setopt($chz, CURLOPT_SSL_VERIFYPEER,false);
										curl_setopt($chz, CURLOPT_MAXREDIRS, 10);
										curl_setopt($chz, CURLOPT_FOLLOWLOCATION, 1);
										curl_setopt($chz, CURLOPT_CONNECTTIMEOUT, $timeout);
										$zremvdata = curl_exec($chz);

										//print_r($productData);
										//exit;
										curl_close($chz);
										
										$productData = json_decode($zremvdata, true);
										
										if(!empty($productData))
										{
											$client->call($session, 'northstar_zonalpricing.addzonalprices', array($productData));

										}
										
										

									}

									

									/*end of Zone price */

					
				       				$oldimg= $client->call($session, 'catalog_product_attribute_media.list', $_product->getSku());
				      				

				       				foreach($oldimg as $img){
					
										try{
										$resultr=$client->call($session, 'catalog_product_attribute_media.remove', array('product'=>$_product->getSku(), 'file'=>$img['file']));
										//$client->catalogProductAttributeMediaRemove((object)array('sessionId' => $session->result, 'productId' =>$_product->getSku(), 'file' => $img['file']));
									
									  
										}catch(exception $e){
											Mage::log($e->getMessage(), 7, $this->dataSyncLogFile, true);
										}
					
				       				}
					 
					
									// code for copy image

				       				$product_images = $clientiron->call($sessionId, 'catalog_product_attribute_media.list', $_product->getSku());


									if(count($product_images) > 0)
									{

										for($i=0;$i < count($product_images) ;$i++){
											unset($product_images[$i]['file']);
											$curl = curl_init($product_images[$i]['url']);

											if (!exif_imagetype($product_images[$i]['url']) === false)
											{
												curl_setopt($curl, CURLOPT_RETURNTRANSFER, true );
												$ret_val = curl_exec($curl);
													if(!curl_errno($curl)){
														$file = array(
															'content' => chunk_split(base64_encode($ret_val)),
															'mime' => curl_getinfo($curl , CURLINFO_CONTENT_TYPE),
														);

														$product_images[$i]['file']=$file;
														$client->call($session,"catalog_product_attribute_media.create", array($_product->getSku(), $product_images[$i]));

													}
												curl_close($curl);
											}
										}
									}


									$clientiron->call($sessionId, 'catalog_product.update',array($_product->getSku(), array('syncpro'=>0)));



								/*	foreach($newImages as $img){

											$mpath= $mediapath."catalog/product".$img['file'];
											$flnm=end(explode("/" , $img['file'] ));
											$fnm=explode("." , $flnm);
											$im = file_get_contents($mpath);
											$imdata = base64_encode($im);
												
												$file = array(
													//'name' => $fnm[0], 
													'content' => "'".$imdata."'",
													'mime' => "'image/".$fnm[1]."'"
													);

											try{
											//$client->call($session, 'catalog_product_attribute_media.remove', array('product'=>$_product->getSku(), 'file'=>$img['file']));
											
											$client->call($session, 'catalog_product_attribute_media.create',array($_product->getSku(),array('file'=>$file, 'label' => $img['label'], 'position' => $img['position'], 'types' => array('image'), 'exclude' => $img['disabled'])));
											
											}catch(exception $e){
											}
											
											//echo "<pre>";
											//print_r($img);
											//print_r($file);
											//exit;
								
									} */
								
					 
					
					
									// if($result)
									// {
									// 	$_product->setData('syncpro', 0)->save();
									// 	//$result2 = $clientiron->call($sessionId, 'catalog_product.update',array($_product->getSku(), array('syncpro'=>0)));
										
										
										
									// 	$connectionRead = Mage::getSingleton('core/resource')->getConnection('core_read');
									// 	$connectionWrite = Mage::getSingleton('core/resource')->getConnection('core_write');
										
									// 	$productId = $_product->getId();
									// 	$productSku = $_product->getSku();
									// 	$videoPath = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'catalog/product/videos/'.$productId.'/';
										
									// 	$ifuturz_productvideoTable = 'ifuturz_productvideo';
										
									// 	$select = $connectionRead->select()
									// 			->from($ifuturz_productvideoTable, 'video_name')
									// 			->where('product_id=?',$productId);
									// 	$videoName = $connectionRead->fetchOne($select);
										
									// 	$videoPathUrl = $videoPath.$videoName;
										
									// 	$embededvideo = urlencode($_product->getProductVideo());
										
									// 	$url = 'http://proudsheep.northstarhosting.com/videoSync/videoSync.php?productsku='.$productSku.'&videopath='.$videoPathUrl.'&videoname='.$videoName.'&embededvideo='.$embededvideo;
									// 	$ch = curl_init();
									// 	$timeout = 5;
									// 	curl_setopt($ch, CURLOPT_URL, $url);
									// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
									// 	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
									// 	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
									// 	curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
									// 	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
									// 	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
									// 	$data = curl_exec($ch);
									// 	//echo $data;
									// 	//exit;
									// 	curl_close($ch);
										
										
										
										
										
									// 	//Mage::getSingleton('adminhtml/session')->addSuccess('Synchronization succesfully completed');			
									// 	//$this->_redirect('adminhtml/catalog_product/index');		
									// }else{			
									// 	Mage::getSingleton('adminhtml/session')->addError('Synchronization unsuccesfull');
									// 	$this->_redirect('adminhtml/catalog_product/index');
									// }
					
					
								}
							}else{
							//Mage::getSingleton('adminhtml/session')->addError('No product for sync');
							$this->_redirect('adminhtml/catalog_product/index');  
				
							}
						}
				
						$i++;	

						$totProduct++;	
					}

					
					$totProdMsg = ($totProduct / count($activeStoreArr))." Product(s) synchronization succesfully completed";

					Mage::getSingleton('adminhtml/session')->addSuccess($totProdMsg);			
					$this->_redirect('adminhtml/catalog_product/index');

					$logDetails = implode(",",$productSyncArr);
					$syncTime = Mage::getSingleton('core/session')->getSyncStartTime();
					$syncEndTime = Mage::getModel('core/date')->date('Y-m-d H:i:s');

					$data = array('sync_from'=>'PMT','sync_to'=>'Production','log_details'=>$logDetails,'created_time'=>$syncTime,'finish_time'=>$syncEndTime);
					$model = Mage::getModel('datasync/datasync')->setData($data);
					try {
						    $model->save();
					} catch (Exception $e){
						echo $e->getMessage();
						Mage::log($e->getMessage(), 7, $this->dataSyncLogFile, true);
					}
						
					//added here for limit to redirect again to function
					//$this->_redirect('*/*/syncprocess');
					$syncactionValue = $this->getRequest()->getparam('syncaction');
					$keyValue = $this->getRequest()->getparam('key');
					$redirectLocationUrl = Mage::getBaseUrl().'datasync/adminhtml_datasync/syncprocess/syncaction/'.$syncactionValue.'/key/'.$keyValue;
						
					header('Location: '.$redirectLocationUrl);

				}
			}else{

					$logDetails = "No product has been synced.";
					//$syncTime = Mage::getSingleton('core/session')->getSyncStartTime();
					$syncEndTime = Mage::getModel('core/date')->date('Y-m-d H:i:s');

					$data = array('sync_from'=>'PMT','sync_to'=>'Production','log_details'=>$logDetails,'created_time'=>$syncEndTime,'finish_time'=>$syncEndTime);
					$model = Mage::getModel('datasync/datasync')->setData($data);
					try {
						    $model->save();
					} catch (Exception $e){
						echo $e->getMessage();
						Mage::log($e->getMessage(), 7, $this->dataSyncLogFile, true);
					}
				//Mage::getSingleton('adminhtml/session')->addError('No product for sync');
				Mage::getSingleton('core/session')->addNotice('There is no product for Synchronization');
				$this->_redirect('adminhtml/catalog_product/index');
			}
				
       	}
       
       
       
       
    public function syncproductprocessAction()
    {
		// $client = new SoapClient('http://proudsheep.northstarhosting.com/index.php/api/soap/?wsdl');
		// $session = $client->login('thunderuser', 'thunderapikey');
	
		// $clientiron = new SoapClient('http://leftshark.northstarhosting.com/index.php/api/soap/?wsdl');
		// $sessionId = $clientiron->login('ironuser', 'ironapikey');

	     //$clientiron = new SoapClient('http://leftshark.northstarhosting.com/index.php/api/soap/?wsdl');
		//$sessionId = $clientiron->login('ironuser', 'ironapikey');

		$clientironurl = Mage::getStoreConfig('datasync/datasync_group/datasync_firstapiurl');

		$ironusername = Mage::getStoreConfig('datasync/datasync_group/datasync_firstapiusername');
		$ironpass = Mage::getStoreConfig('datasync/datasync_group/datasync_firstapipass');

		$clientiron = new SoapClient($clientironurl);
		$sessionId = $clientiron->login($ironusername, $ironpass);

   		//$client = new SoapClient('http://proudsheep.northstarhosting.com/index.php/api/soap/?wsdl');
		//$session = $client->login('thunderuser', 'thunderapikey');


		$clienturl = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapiurl');

		$clientusername = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapiusername');
		$clientpass = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapipass');

		$client = new SoapClient($clienturl);
		$session = $client->login($clientusername, $clientpass);
		
		
		$curlUrlProduction = Mage::getStoreConfig('datasync/datasync_group/datasync_secondweburl');


		$this->createZoneListToProduction();


		//$mediapath=Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
		$mediapath= $curlUrlProduction."/media/";
		$storelist = $client->call($session, 'store.list');
	
		$productSyncArr = array();
		$activeStoreArr = array();
		$activeStoreArr[] = 0;

		foreach($storelist as $storedetail){		
			$activeStore = $storedetail['is_active'];
			if($activeStore){			
				$activeStoreArr[] = $storedetail['store_id'];
			}
		}
	
		$filters = array(array('syncpro' => array(1)));	
		$products = $client->call($session, 'product.list', $filters);
		
		$syncLimit = Mage::getStoreConfig('datasync/datasync_group/datasync_maxlimit');
		$j= 0;
		$syncPageLimit = ceil((count($products) / $syncLimit));


		// if(count($products) > 0 && $syncLimit > count($products))
		// {
		// 		$syncPageLimit = 1;
		// }

		$totProduct = 0;
	
		if(count($products)>0){	

			$syncProdStartTime = Mage::getModel('core/date')->date('Y-m-d H:i:s');

			Mage::getSingleton('core/session')->setProdSyncStartTime($syncProdStartTime);
		
			for($j = 0; $j < $syncPageLimit; $j++)
			{
		
				foreach($activeStoreArr as $storeid){
			
				//$storeid=$storedetail['store_id'];
				//$activeStore = $storedetail['is_active'];

					$zoneflag = "no";
			
					foreach($products as $_product){
			  
					
						if (!in_array($_product['sku'], $productSyncArr))
						{
					 		$productSyncArr[] = $_product['sku'];

						}

						$id = Mage::getModel('catalog/product')->getResource()->getIdBySku($_product['sku']);
						// if($_product['sku']!=""){
						$product_details=$client->call($session, 'product.info',array($_product['sku'],$storeid));	

						unset($product_details['group_price']);
						unset($product_details['tier_price']);

						$product_details['syncpro']=0;
						//  }
				
				
						$newImages = $client->call($session, 'catalog_product_attribute_media.list', $_product['sku']);
				
						//echo "<pre>";
						//print_r($newImages);
						//exit;
					
						//$inventroy=$client->call($session, 'cataloginventory_stock_item.list', $_product['sku']);
				
						if($id){

							try{
								$result = $clientiron->call($sessionId, 'catalog_product.update',array($_product['sku'], $product_details, $storeid));
							}catch(exception $e){
								Mage::log($e->getMessage(), 7, $this->dataSyncLogFile, true);
							}


							// if($result)
							// {



							// }

							$wid = Mage::getModel('core/store')->load($storeid)->getWebsiteId();

							if($result)
							{
								if($storeid == 0)
								{


								$productData = array();

								$pid = $product_details['product_id'];

								$zurl = $curlUrlProduction.'/zonepricesync/zonepricesync.php?productid='.$pid.'&syncpid='.$id;
								$chs = curl_init();
								$timeout = 5;
								curl_setopt($chs, CURLOPT_URL, $zurl);
								curl_setopt($chs, CURLOPT_RETURNTRANSFER, 1);
								curl_setopt($chs, CURLOPT_SSL_VERIFYHOST,false);
								curl_setopt($chs, CURLOPT_SSL_VERIFYPEER,false);
								curl_setopt($chs, CURLOPT_MAXREDIRS, 10);
								curl_setopt($chs, CURLOPT_FOLLOWLOCATION, 1);
								curl_setopt($chs, CURLOPT_CONNECTTIMEOUT, $timeout);
								$zdata = curl_exec($chs);

								//print_r($productData);
								//exit;
								curl_close($chs);

								$productData = json_decode($zdata, true);



								//$zonewebId = $productData['tier_price'][0]['website_id'][0];


								//$wid = Mage::getModel('core/store')->load($storeid)->getWebsiteId();

									if(!empty($productData))
									{
										$a = $clientiron->call($sessionId, 'northstar_zonalpricing.addzonalprices', array($productData));

									}else{

										$zproduct = Mage::getModel('catalog/product')->load($id);

										$delzonePriceCollections =  $zproduct->getZonalPrice();
										$delzoneSpecialPriceCollections = $zproduct->getZonalSpecialPrice();
										$delzoneGroupPriceCollection = $zproduct->getZonalGroupPrice();
										$delzoneTierPriceCollection = $zproduct->getZonalTierPrice();

										$delzonePriceFlag = 'no';

										$productData['product_id'] = $id;

										if(count($delzoneGroupPriceCollection) > 0)
										{
											$delzonePriceFlag = 'yes';

											foreach($delzoneGroupPriceCollection as $delgroupPriceCollection)
											{
												if($delgroupPriceCollection['zone_code'] == 'default')
												{
													$productData['group_price'][] =  array(
																							'website_id' => $delgroupPriceCollection['website_id'],
																							'cust_group' => $delgroupPriceCollection['cust_group'],
																							'zone' => 0,
																							'price' => $delgroupPriceCollection['price'],
																							'delete' => 1
																							);


												}else{
													$pushArr = array('delete' => 1);
													$delgroupPriceCollection = array_merge($delgroupPriceCollection,$pushArr);
													$productData['group_price'][] =  $delgroupPriceCollection;
													
												}
												

											}

										}



										if(count($delzoneTierPriceCollection) > 0)
										{
											$delzonePriceFlag = 'yes';
											//omit checking another server has or not zone price setup, due to API not provide.

											foreach($delzoneTierPriceCollection as $deltierPriceCollection)
											{
												$tierType = $deltierPriceCollection['zone_code'];
												if($tierType == 'default')
												{
													$productData['tier_price'][] = array(
																						'website_id' => $deltierPriceCollection['website_id'],
																						'cust_group' => $deltierPriceCollection['cust_group'],
																						'zone' => 0,
																						'price_qty' => $deltierPriceCollection['price_qty'],
																						'price' => $deltierPriceCollection['price'],
																						'delete' => 1

																						);


												}else{
													$pushArr = array('delete' => 1);
													$deltierPriceCollection = array_merge($deltierPriceCollection,$pushArr);
													$productData['tier_price'][] = $deltierPriceCollection;
												}
												
												
											}

										}


										if(count($delzonePriceCollections) > 0)
										{
											$delzonePriceFlag = 'yes';
											//omit checking another server has or not zone price setup, due to API not provide.
											//$productData['zonal_price'] = array();

											foreach($delzonePriceCollections as $delzonePriceCollection)
											{
												
												$productData['zonal_price'][] = 
																				array( 
																					'zone_code' => $delzonePriceCollection['zone_code'],
																					'price' => $delzonePriceCollection['price'],
																					'delete' => 1

																					);
																				


											}

										}

										if(count($delzoneSpecialPriceCollections) > 0)
										{
											$delzonePriceFlag = 'yes';
											//omit checking another server has or not zone price setup, due to API not provide.
											//$productData['group_price'] = array();

											foreach($delzoneSpecialPriceCollections as $delzoneSpecialPriceCollection)
											{
												
												$productData['zonal_special_price'][] =

																						array( 
																							'zone_code' => $delzoneSpecialPriceCollection['zone_code'],
																							'special_price' => $delzoneSpecialPriceCollection['price'],
																							'delete' => 1

																							);
																				


											}

										}


										if($delzonePriceFlag == 'yes')
										{
											 $clientiron->call($sessionId, 'northstar_zonalpricing.addzonalprices', array($productData));


											// $delZonePriceProduct = Mage::getModel('catalog/product')->load($id);

											// $delZonePriceProduct->setZonalGroupPrice($productData['group_price']);
										 //    $delZonePriceProduct->setZonalTierPrice($productData['tier_price']);
										 //    $delZonePriceProduct->setZonalPrice($productData['zonal_price']);
										 //    $delZonePriceProduct->setZonalSpecialPrice($productData['zonal_special_price']);
										 //    $delZonePriceProduct->save();
										}

									}


								
							}
						}

					
							try{

								$oldImages = $clientiron->call($sessionId, 'catalog_product_attribute_media.list', $_product['sku']);	
						
								foreach($oldImages as $img){
						
									$resultr=$clientiron->call($sessionId, 'catalog_product_attribute_media.remove', array('product'=>$_product['sku'], 'file'=>$img['file']));
								//$client->catalogProductAttributeMediaRemove((object)array('sessionId' => $session->result, 'productId' =>$_product->getSku(), 'file' => $img['file']));

								}
							}catch(exception $e){
								Mage::log($e->getMessage(), 7, $this->dataSyncLogFile, true);
							}


							// code for copy image

	       					$product_images = $client->call($session, 'catalog_product_attribute_media.list', $_product['sku']);

							if(count($product_images) > 0)
							{

								for($i=0;$i < count($product_images) ;$i++)
								{
									unset($product_images[$i]['file']);
									$curl = curl_init($product_images[$i]['url']);
									
									if (!exif_imagetype($product_images[$i]['url']) === false)
									{
										curl_setopt($curl, CURLOPT_RETURNTRANSFER, true );
										$ret_val = curl_exec($curl);

										if(!curl_errno($curl))
										{
											$file = array(
												'content' => chunk_split(base64_encode($ret_val)),
												'mime' => curl_getinfo($curl , CURLINFO_CONTENT_TYPE),
											);
										
											$product_images[$i]['file']=$file;
											$result2 = $clientiron->call($sessionId,"catalog_product_attribute_media.create", array($_product['sku'], $product_images[$i]));

										}
										
										curl_close($curl);
									}
								}

							}
					 

							if($result)
							{

							
								try{
									$client->call($session, 'catalog_product.update',array($_product['sku'], array('syncpro'=>0)));
								}catch(exception $e){
									Mage::log($e->getMessage(), 7, $this->dataSyncLogFile, true);
								}


								//Mage::getSingleton('adminhtml/session')->addSuccess('Synchronization succesfully completed');			
								//$this->_redirect('adminhtml/catalog_product/index');			
							}else{			
								Mage::getSingleton('adminhtml/session')->addError('Synchronization unsuccesfull');
								$this->_redirect('adminhtml/catalog_product/index');
							}   
					
					
						}else{
							$attributeSets = $clientiron->call($sessionId, 'product_attribute_set.list');
							$attributeSet = current($attributeSets);	   
							$result = $clientiron->call($sessionId, 'catalog_product.create', array($_product['type'], $attributeSet['set_id'], $_product['sku'], $product_details, $storeid));
							//$clientiron->call($sessionId,'product_stock.update',array($_product['sku'], $inventroy[0]));
						
						  if($result)
						  {
							
							if($_product['type'] == 'grouped')
							{
								$associateSku = array();
								$newProductSku = array();
								$associatedProduct= $client->call($session, 'catalog_product_link.list', array('type' => 'grouped', 'product' => $_product['sku']));

								//$associatedProduct = $_product->getTypeInstance(true)->getAssociatedProducts($_product);
	
								foreach($associatedProduct as $associatedPro)
								{
									// $id = Mage::getModel('catalog/product')->getResource()->getIdBySku($associatedPro['sku']);


									// // if($_product['sku']!=""){

									// if($id)
									// {
									// 	$associateSku[] =  $associatedPro['sku'];
									// }else{
									// 	$newProductSku[] =  $associatedPro['sku'];
									// }


										$groupProductExist = null;
											//$groupProductNotExist = null;
									
											try{
													$groupProductExist = $clientiron->call($sessionId, 'product.info', array($associatedPro['sku']));
													//$groupProductExist = $clientiron->call($sessionId, 'product.info', array('SKU-TEST-SIMPLE-1'));

											}
											catch(exception $e){
												//nothing to do
												Mage::log($e->getMessage(), 7, $this->dataSyncLogFile, true);
											}

											//print_r($groupProductExist);

											//echo count($groupProductExist);

											 if(!$groupProductExist==null || !$groupProductExist=="")
											 {
											 	$associateSku[] =  $associatedPro['sku'];
											 }else{
											 	$newProductSku[] =  $associatedPro['sku'];
											 }

											 // if($groupProductExist==null or $groupProductExist=="")
											 // {
											 // 	$newProductSku[] =  $associatedPro['sku'];
											 // }



								}


								if(!empty($newProductSku))
								{
											
									foreach($newProductSku as $simpleProdSku)
									{
										foreach ($activeStoreArr as $storeId)
										{
											$product_detailsg==null;



											$id = Mage::getModel('catalog/product')->getResource()->getIdBySku($simpleProdSku);

											try{
												$product_detailsg=$client->call($session, 'product.info',array($simpleProdSku,$storeId));

											    $product_detailsg['syncpro']=0;
											}
											catch(exception $e){
												//nothing to do
												Mage::log($e->getMessage(), 7, $this->dataSyncLogFile, true);
											}


											if($id)
											{
												$clientiron->call($sessionId, 'catalog_product.update',array($simpleProdSku, $product_detailsg, $storeId));

												$oldimg= $clientiron->call($sessionId, 'catalog_product_attribute_media.list', $simpleProdSku);
				      				

				      							if($oldimg > 0)
				      							{
							
													foreach($oldimg as $img){
												
														try{
															$resultr=$clientiron->call($sessionId, 'catalog_product_attribute_media.remove', array('product'=>$simpleProdSku, 'file'=>$img['file']));
															//$client->catalogProductAttributeMediaRemove((object)array('sessionId' => $session->result, 'productId' =>$_product->getSku(), 'file' => $img['file']));
																
																  
															}catch(exception $e){
																Mage::log($e->getMessage(), 7, $this->dataSyncLogFile, true);
														}
					
				       								}
				       							}
					 

												$product_images = $client->call($session, 'catalog_product_attribute_media.list', $simpleProdSku);

								
												if(count($product_images) > 0)
												{
			
													for($i=0;$i < count($product_images) ;$i++){
														unset($product_images[$i]['file']);
														$curl = curl_init($product_images[$i]['url']);
			
														if (!exif_imagetype($product_images[$i]['url']) === false)
														{
															curl_setopt($curl, CURLOPT_RETURNTRANSFER, true );
															$ret_val = curl_exec($curl);
																if(!curl_errno($curl)){
																	$file = array(
																		'content' => chunk_split(base64_encode($ret_val)),
																		'mime' => curl_getinfo($curl , CURLINFO_CONTENT_TYPE),
																	);
			
																	$product_images[$i]['file']=$file;
																	$clientiron->call($sessionId,"catalog_product_attribute_media.create", array($simpleProdSku, $product_images[$i]));
			
																}
															curl_close($curl);
														}
													}
												}

											}else{
												$simpleAttributeSets = $clientiron->call($sessionId, 'product_attribute_set.list');
												$simpleAttributeSet = current($simpleAttributeSets);	

												$result = $clientiron->call($sessionId, 'catalog_product.create', array($associatedPro['type'], $simpleAttributeSet['set_id'], $simpleProdSku, $product_detailsg, $storeId));


											}




											// if(!$product_detailsg==null || !$product_detailsg=="")
											// {

											// 	$simpleAttributeSets = $clientiron->call($sessionId, 'product_attribute_set.list');
											// 	$simpleAttributeSet = current($simpleAttributeSets);	

											// 	$result = $clientiron->call($sessionId, 'catalog_product.create', array($associatedPro['type'], $simpleAttributeSet['set_id'], $simpleProdSku, $product_detailsg, $storeId));

											// 	//$associateSku[] =  $simpleProdSku;
											// 	//unset($newProductSku[$simpleProdSku]);
	
											// }else{

											// 	$clientiron->call($sessionId, 'catalog_product.update',array($simpleProdSku, $product_detailsg, $storeId));
											// 	//$clientiron->call($sessionId, 'catalog_product.update',array($groupSimpleProduct->getSku(), $data, $storeId));
											// }
										}

										//$product = Mage::getModel('catalog/product')->loadByAttribute('sku',$simpleProdSku);

										//$simpleAttributeSets = $client->call($session, 'product_attribute_set.list');
										//$simplettributeSet = current($simpleAttributeSets);		    
										//$result = $client->call($session, 'catalog_product.create', array($_product->getTypeID(), $simplettributeSet['set_id'], $simpleProdSku, $data, $_storeId));
									
										if (!in_array($simpleProdSku, $associateSku))
										{
					  						$associateSku[] =  $simpleProdSku;
										}


									}
											
									//$associateSku[] =  $simpleProdSku;
								}

								// for($i=0; $i < count($associatedProduct); $i++)
								// {
								// 	echo $associatedProduct['sku'];
								// }


								//echo "<pre>";
								//print_r($associatedProduct->getSku());
							

								if(count($associateSku > 0))
								{
									//$commaSepSku = implode(",",$associateSku);
									//$commaSepSku = trim($commaSepSku);

									foreach($associateSku as $assoSku)
									{
										$clientiron->call($sessionId, 'catalog_product_link.assign', array('type' => 'grouped', 'sku' => $_product['sku'], 'linkedProduct' => $assoSku));
									}

								}

							}
						  }

		

				
		

							/*
							foreach($newImages as $img){
						
								$mpath= $mediapath."catalog/product/".$img['file'];
								$flnm=end(explode("/" , $img['file'] ));
								$fnm=explode("." , $flnm);
											
								$file = array(
									'name' => $fnm[0], 
									'content' => base64_encode(file_get_contents($mpath)),
									'mime' => 'image/'.$fnm[1]
								);
									
								try{
									//$client->call($session, 'catalog_product_attribute_media.remove', array('product' =>$_product->getSku(), 'file'=>$img['file']));
									$client->call($session, 'catalog_product_attribute_media.create',array( $_product->getSku(),array('file'=>$file, 'label' => $img['label'], 'position' => $img['position'], 'types' =>$img['types'], 'exclude' => $img['disabled']),$_storeId));
										
								}catch(exception $e){

								}
						
							}
							*/	
						
						// code for copy image

	       				$product_images = $client->call($session, 'catalog_product_attribute_media.list', $_product['sku']);

						for($i=0;$i < count($product_images) ;$i++){
							unset($product_images[$i]['file']);
							$curl = curl_init($product_images[$i]['url']);
							curl_setopt($curl, CURLOPT_RETURNTRANSFER, true );
							$ret_val = curl_exec($curl);
								if(!curl_errno($curl)){
									$file = array(
										'content' => chunk_split(base64_encode($ret_val)),
										'mime' => curl_getinfo($curl , CURLINFO_CONTENT_TYPE),
									);
									$product_images[$i]['file']=$file;
									$result2 = $clientiron->call($sessionId,"catalog_product_attribute_media.create", array($_product['sku'], $product_images[$i]));

								}
							curl_close($curl);
						}

						

						/*
						if(!empty($newImages)){
						
							foreach($newImages as $img){
					
								//$mpath= $mediapath."catalog/product".$img['file'];
								$mpath= $img['url'];
								$flnm=end(explode("/" , $img['file'] ));
								$fnm=explode("." , $flnm);
								
								$file = array(
									'name' => $fnm[0], 
									'content' => base64_encode(file_get_contents($mpath)),
									'mime' => 'image/'.$fnm[1]
									);
									
								$imgarray=array('file'=>$file, 'label' => $img['label'], 'position' => $img['position'], 'types' => $img['types'], 'exclude' => $img['exclude']);
									
								try{
									//$clientiron->call($sessionId, 'catalog_product_attribute_media.remove', array('product'=>$_product['sku'], 'file'=>$img['file']));
									$result1 = $clientiron->call($sessionId, 'catalog_product_attribute_media.create',array($_product['sku'],$imgarray,$storeid));
								}catch(exception $e){
								}
								
								
							}  
						} */
					
					
					
						if($result)
						{
							/*$connectionRead = Mage::getSingleton('core/resource')->getConnection('core_read');
							$connectionWrite = Mage::getSingleton('core/resource')->getConnection('core_write');
							
							$productSku = $_product['sku'];
							$productId = Mage::getModel('catalog/product')->getResource()->getIdBySku($productSku);
							$videoPath = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'catalog/product/videos/'.$productId.'/';
							
							$ifuturz_productvideoTable = 'ifuturz_productvideo';
							
							$select = $connectionRead->select()
									->from($ifuturz_productvideoTable, 'video_name')
									->where('product_id=?',$productId);
							$videoName = $connectionRead->fetchOne($select);
							
							$videoPathUrl = $videoPath.$videoName;
							
							$_productVideo = Mage::getModel('catalog/product')->load($productId);
							$embededvideo = urlencode($_productVideo->getProductVideo());
							
							$url = 'http://ironhorse.northstarhosting.com/videoSync/videoSync.php?productsku='.$productSku.'&videopath='.$videoPathUrl.'&videoname='.$videoName.'&embededvideo='.$embededvideo;
							$ch = curl_init();
							$timeout = 5;
							curl_setopt($ch, CURLOPT_URL, $url);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
							curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
							curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
							curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
							$data = curl_exec($ch);
							//echo $data;
							//exit;
							curl_close($ch);*/

							//$_product->setData('syncpro', 0)->save();
							$client->call($session, 'catalog_product.update',array($_product['sku'], array('syncpro'=>0)));
							//Mage::getSingleton('adminhtml/session')->addSuccess('Synchronization succesfully completed');
							//$this->_redirect('adminhtml/catalog_product/index');

							//$_product->setSyncpro(0)->save();
							//$client->call($session, 'catalog_product.update',array($_product['sku'], array('syncpro'=>0)));
							//Mage::getSingleton('core/session')->addSuccess('Synchronization succesfully completed');
							//$this->_redirect('*/*/');
						
						}else{		
							Mage::getSingleton('core/session')->addError('Synchronization unsuccesfull');
							$this->_redirect('adminhtml/catalog_product/index');
						}
					
					
					}
				
						$totProduct++;

				}


				
		
			}
					$totProdMsg = ($totProduct / count($activeStoreArr))." Product(s) synchronization succesfully completed";

				Mage::getSingleton('adminhtml/session')->addSuccess($totProdMsg);			
				$this->_redirect('adminhtml/catalog_product/index');

				$logDetails = implode(",",$productSyncArr);

				$syncProdTime = Mage::getSingleton('core/session')->getProdSyncStartTime();
				$syncProdEndTime = Mage::getModel('core/date')->date('Y-m-d H:i:s');

				$data = array('sync_from'=>'Production','sync_to'=>'PMT','log_details'=>$logDetails,'created_time'=>$syncProdTime,'finish_time'=>$syncProdEndTime);
				$model = Mage::getModel('datasync/datasync')->setData($data);
				try {
					    $model->save();
				} catch (Exception $e){
					echo $e->getMessage();  
					Mage::log($e->getMessage(), 7, $this->dataSyncLogFile, true);
				}
				
				//added here for limit to redirect again to function
				//$this->_redirect('*/*/syncprocess');
				//$syncactionValue = $this->getRequest()->getparam('syncaction');
				$keyValue = $this->getRequest()->getparam('key');
				$redirectLocationUrl = Mage::getBaseUrl().'datasync/adminhtml_datasync/syncproductprocess/key/'.$keyValue;
				
				header('Location: '.$redirectLocationUrl);

			}

		}else{		
			Mage::getSingleton('core/session')->addNotice('There is no product for Synchronization');

			$logDetails = "No product has been synced.";
			//$syncTime = Mage::getSingleton('core/session')->getSyncStartTime();
			$syncEndTime = Mage::getModel('core/date')->date('Y-m-d H:i:s');

			$data = array('sync_from'=>'Production','sync_to'=>'PMT','log_details'=>$logDetails,'created_time'=>$syncEndTime,'finish_time'=>$syncEndTime);
			$model = Mage::getModel('datasync/datasync')->setData($data);
			try {
				    $model->save();
			} catch (Exception $e){
				echo $e->getMessage();   
				Mage::log($e->getMessage(), 7, $this->dataSyncLogFile, true);
			}

			$this->_redirect('adminhtml/catalog_product/index');
		}
    }


    public function createZoneListToProduction()
    {
    	$clientironurl = Mage::getStoreConfig('datasync/datasync_group/datasync_firstapiurl');

		$ironusername = Mage::getStoreConfig('datasync/datasync_group/datasync_firstapiusername');
		$ironpass = Mage::getStoreConfig('datasync/datasync_group/datasync_firstapipass');

		$clientiron = new SoapClient($clientironurl);
		$sessionId = $clientiron->login($ironusername, $ironpass);


		$clienturl = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapiurl');

		$clientusername = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapiusername');
		$clientpass = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapipass');

		$client = new SoapClient($clienturl);
		$session = $client->login($clientusername, $clientpass);


		$getZoneListOtherServer = $client->call($session, 'northstar_zonalpricing.getZones');

		$otherZone = array();
		foreach($getZoneListOtherServer as $otherServer)
		{

			$otherZone[] = $otherServer['zone_code'];

		}

		$getZoneList = Mage::getModel('northstar_zonalpricing/zonalpricing')->getCollection()->getData();


		foreach($getZoneList as $zoneList)
		{
			$zone = "'".$zoneList['zone_code']."'";

			if (!in_array($zoneList['zone_code'], $otherZone)) 
			{
			    $zoneData = array('name' => $zoneList['name'],
						        'zone_code' => $zoneList['zone_code'],
						        'countries' => $zoneList['countries'],
						        'status' => $zoneList['status'], 
						         ); 

			    $client->call($session, 'northstar_zonalpricing.setZones', array($zoneData));

			}
		}



		//check for LS
		$getZoneListOtherServer = $client->call($session, 'northstar_zonalpricing.getZones');

		if(count($getZoneListOtherServer) > 0)
		{	
			$otherZone = array();

			$getZoneList = Mage::getModel('northstar_zonalpricing/zonalpricing')->getCollection()->getData();


			foreach($getZoneList as $otherServerzone)
			{

				$otherZone[] = $otherServerzone['zone_code'];

			}


			// foreach($getZoneList as $otherServer)
			// {

			// 	$otherZone[] = $otherServer['zone_code'];

			// }

			
			foreach($getZoneListOtherServer as $zoneList)
			{
				$zone = "'".$zoneList['zone_code']."'";

				if (!in_array($zoneList['zone_code'], $otherZone)) 
				{
				    $zoneData = array('name' => $zoneList['name'],
							        'zone_code' => $zoneList['zone_code'],
							        'countries' => $zoneList['countries'],
							        'status' => $zoneList['status'], 
							         ); 

				    $clientiron->call($sessionId, 'northstar_zonalpricing.setZones', array($zoneData));

				}
			}
		}

    }


  //   public function createZoneListToPManagement()
  //   {

  //   	$clientironurl = Mage::getStoreConfig('datasync/datasync_group/datasync_firstapiurl');

		// $ironusername = Mage::getStoreConfig('datasync/datasync_group/datasync_firstapiusername');
		// $ironpass = Mage::getStoreConfig('datasync/datasync_group/datasync_firstapipass');

		// $clientiron = new SoapClient($clientironurl);
		// $sessionId = $clientiron->login($ironusername, $ironpass);


		// $clienturl = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapiurl');

		// $clientusername = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapiusername');
		// $clientpass = Mage::getStoreConfig('datasync/datasync_group/datasync_secondapipass');

		// $client = new SoapClient($clienturl);
		// $session = $client->login($clientusername, $clientpass);


		// $getZoneListOtherServer = $client->call($session, 'northstar_zonalpricing.getZones');

		// if(count($getZoneListOtherServer) > 0)
		// {	
		// 	$otherZone = array();

		// 	$getZoneList = Mage::getModel('northstar_zonalpricing/zonalpricing')->getCollection()->getData();


		// 	foreach($getZoneList as $otherServerzone)
		// 	{

		// 		$otherZone[] = $otherServerzone['zone_code'];

		// 	}


		// 	// foreach($getZoneList as $otherServer)
		// 	// {

		// 	// 	$otherZone[] = $otherServer['zone_code'];

		// 	// }

			
		// 	foreach($getZoneListOtherServer as $zoneList)
		// 	{
		// 		$zone = "'".$zoneList['zone_code']."'";

		// 		if (!in_array($zoneList['zone_code'], $otherZone)) 
		// 		{
		// 		    $zoneData = array('name' => $zoneList['name'],
		// 					        'zone_code' => $zoneList['zone_code'],
		// 					        'countries' => $zoneList['countries'],
		// 					        'status' => $zoneList['status'], 
		// 					         ); 

		// 		    $clientiron->call($sessionId, 'northstar_zonalpricing.setZones', array($zoneData));

		// 		}
		// 	}
		// }

  //   }

}