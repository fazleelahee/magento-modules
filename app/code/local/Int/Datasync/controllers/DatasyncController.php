<?php

class Int_Datasync_Adminhtml_DatasyncController extends Mage_Adminhtml_Controller_action
{

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('datasync/items')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));

        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('datasync/datasync')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('datasync_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('datasync/items');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('datasync/adminhtml_datasync_edit'))
                ->_addLeft($this->getLayout()->createBlock('datasync/adminhtml_datasync_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('datasync')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {

            if (isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
                try {
                    /* Starting upload */
                    $uploader = new Varien_File_Uploader('filename');

                    // Any extention would work
                    $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                    $uploader->setAllowRenameFiles(false);

                    // Set the file upload mode
                    // false -> get the file directly in the specified folder
                    // true -> get the file in the product like folders
                    //	(file.jpg will go in something like /media/f/i/file.jpg)
                    $uploader->setFilesDispersion(false);

                    // We set media as the upload dir
                    $path = Mage::getBaseDir('media') . DS;
                    $uploader->save($path, $_FILES['filename']['name']);

                } catch (Exception $e) {

                }

                //this way the name is saved in DB
                $data['filename'] = $_FILES['filename']['name'];
            }


            $model = Mage::getModel('datasync/datasync');
            $model->setData($data)
                ->setId($this->getRequest()->getParam('id'));

            try {
                if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
                    $model->setCreatedTime(now())
                        ->setUpdateTime(now());
                } else {
                    $model->setUpdateTime(now());
                }

                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('datasync')->__('Item was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('datasync')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('datasync/datasync');

                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $datasyncIds = $this->getRequest()->getParam('datasync');
        if (!is_array($datasyncIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($datasyncIds as $datasyncId) {
                    $datasync = Mage::getModel('datasync/datasync')->load($datasyncId);
                    $datasync->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($datasyncIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massStatusAction()
    {
        $datasyncIds = $this->getRequest()->getParam('datasync');
        if (!is_array($datasyncIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($datasyncIds as $datasyncId) {
                    $datasync = Mage::getSingleton('datasync/datasync')
                        ->load($datasyncId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($datasyncIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function exportCsvAction()
    {
        $fileName = 'datasync.csv';
        $content = $this->getLayout()->createBlock('datasync/adminhtml_datasync_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName = 'datasync.xml';
        $content = $this->getLayout()->createBlock('datasync/adminhtml_datasync_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType = 'application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK', '');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }


    public function syncprocessAction()
    {

        //$collection=Mage::getModel('catalog/product')->getCollection()->addAttributeToFilter('syncpro',1);
        echo $syncaction = $this->getRequest()->getparam('syncaction');
        die();
        $clientiron = new SoapClient('http://ironhorse.northstarhosting.com/index.php/api/soap/?wsdl');
        $sessionId = $clientiron->login('ironuser', '7YnAtAVYDA6EReve');
        $filters = array(array('syncpro' => array(1)));
        $mediapath = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        $products = $clientiron->call($sessionId, 'product.list', $filters);


        if (count($products) > 0) {


            $client = new SoapClient('http://thundercat.northstarhosting.com/index.php/api/soap/?wsdl');
            $session = $client->login('thunderuser', 'UTEXENUWULEbYRuv');

            foreach ($products as $product) {

                if ($product['sku'] != "") {

                    $id = Mage::getModel('catalog/product')->getResource()->getIdBySku($product['sku']);

                    $_product = Mage::getModel('catalog/product')->load($id);

                    $categoryIds = $_product->getCategoryIds();
                    $websiteids = $_product->getWebsiteIds();
                    // $catidarr['categories']=$categoryIds;
                    // $catidarr['websites']=$websiteids;


                    $data = $_product->getData();
                    $stockitem = $data['stock_item'];
                    $stockdata = $stockitem->getData();
                    $newImages = $data['media_gallery']['images'];
                    unset($data['stock_item']);
                    $mediapath = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
                    //$data = array_merge($data, $catidarr);

                    // print_r( $data);
                    foreach ($data as $k => $v) {
                        if ($k == 'category_ids') {
                            $cv = $v;
                            unset ($data[$k]);
                            $new_key = 'categories';

                            $data[$new_key] = $cv;
                        }
                        if ($k == 'website_ids') {
                            $cv = $v;
                            unset ($data[$k]);
                            $new_key = 'websites';

                            $data[$new_key] = $cv;
                        }
                        if ($k == 'syncpro') {
                            $data['syncpro'] = 0;
                        }
                    }


                    $product_exist = null;
                    try {
                        $product_exist = $client->call($session, 'product.info', array($_product->getSku()));

                    } catch (exception $e) {
                        //nothing to do
                    }


                    if ($product_exist == null or $product_exist == "") {
                        $attributeSets = $client->call($session, 'product_attribute_set.list');
                        $attributeSet = current($attributeSets);
                        $result = $client->call($session, 'catalog_product.create', array($_product->getTypeID(), $attributeSet['set_id'], $_product->getSku(), $data));
                        $client->call($session, 'product_stock.update', array($_product->getSku(), $stockdata));

                        $resultimg = null;
                        foreach ($newImages as $img) {
                            try {
                                $resultimg = $client->call($session, 'catalog_product_attribute_media.info', array($_product->getSku(), 'file' => $img['file']));

                            } catch (exception $e) {

                            }
                            $mpath = $mediapath . "catalog/product/" . $img['file'];
                            $flnm = end(explode("/", $img['file']));
                            // print_r ($resultimg);
                            if ($resultimg == null or $resultimg == "") {

                                $file = array(
                                    'name' => $flnm,
                                    'content' => base64_encode(file_get_contents($mpath)),
                                    'mime' => 'image/jpeg'
                                );

                                $result1 = $client->call($session, 'catalog_product_attribute_media.create', array($_product->getSku(), array('file' => $file, 'label' => $img['label'], 'position' => $img['position'], 'types' => array('image'), 'exclude' => $img['disabled'])));


                            }

                        }


                        if ($result1) {
                            $_product->setData('syncpro', 0)->save();

                            //$clientiron->call($sessionId, 'catalog_product.update',array($_product->getSku(), array('syncpro'=>0)));
                            Mage::getSingleton('adminhtml/session')->addSuccess('Synchronization succesfully completed');
                            $this->_redirect('adminhtml/catalog_product/index');

                        } else {
                            Mage::getSingleton('adminhtml/session')->addError('Synchronization unsuccesfull');
                            $this->_redirect('adminhtml/catalog_product/index');
                        }


                    } else {

                        $result = $client->call($session, 'catalog_product.update', array($_product->getSku(), $data));

                        $client->call($session, 'product_stock.update', array($_product->getSku(), $stockdata));

                        $resultimg = null;
                        foreach ($newImages as $img) {
                            try {
                                $resultimg = $client->call($session, 'catalog_product_attribute_media.info', array($_product->getSku(), 'file' => $img['file']));

                            } catch (exception $e) {

                            }
                            $mpath = $mediapath . "catalog/product/" . $img['file'];
                            $flnm = end(explode("/", $img['file']));
                            // print_r ($resultimg);
                            if ($resultimg == null or $resultimg == "") {

                                $file = array(
                                    'name' => $flnm,
                                    'content' => base64_encode(file_get_contents($mpath)),
                                    'mime' => 'image/jpeg'
                                );

                                $result1 = $client->call($session, 'catalog_product_attribute_media.create', array($_product->getSku(), array('file' => $file, 'label' => $img['label'], 'position' => $img['position'], 'types' => array('image'), 'exclude' => $img['disabled'])));


                            }

                        }


                        if ($result) {
                            $_product->setData('syncpro', 0)->save();
                            //$result2 = $clientiron->call($sessionId, 'catalog_product.update',array($_product->getSku(), array('syncpro'=>0)));

                            Mage::getSingleton('adminhtml/session')->addSuccess('Synchronization succesfully completed');
                            $this->_redirect('adminhtml/catalog_product/index');
                        } else {
                            Mage::getSingleton('adminhtml/session')->addError('Synchronization unsuccesfull');
                            $this->_redirect('adminhtml/catalog_product/index');
                        }


                    }
                } else {
                    Mage::getSingleton('adminhtml/session')->addError('No product for sync');
                    $this->_redirect('adminhtml/catalog_product/index');

                }
            }
        } else {
            Mage::getSingleton('adminhtml/session')->addError('No product for sync');
            $this->_redirect('adminhtml/catalog_product/index');

        }

        $this->_redirect('adminhtml/catalog_product/index');
    }



    public function syncproductprocessAction()
    {

        $client = new SoapClient('http://thundercat.northstarhosting.com/index.php/api/soap/?wsdl');
        $session = $client->login('thunderuser', 'UTEXENUWULEbYRuv');

        $clientiron = new SoapClient('http://ironhorse.northstarhosting.com/index.php/api/soap/?wsdl');
        $sessionId = $clientiron->login('ironuser', '7YnAtAVYDA6EReve');
        $mediapath = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);

        $filters = array(array('syncpro' => array(1)));

        $products = $client->call($session, 'product.list', $filters);

        if (count($products) > 0) {

            foreach ($products as $_product) {

                $id = Mage::getModel('catalog/product')->getResource()->getIdBySku($_product['sku']);
                // if($_product['sku']!=""){
                $product_details = $client->call($session, 'product.info', array($_product['sku']));
                $product_details['syncpro'] = 0;
                //  }
                // $newImages = $client->call($session, 'catalog_product_attribute_media.info', array($_product['sku']));

                $newImages = $client->call($session, 'catalog_product_attribute_media.list', $_product['sku']);


                $inventroy = $client->call($session, 'cataloginventory_stock_item.list', $_product['sku']);

                if ($id) {

                    $result = $clientiron->call($sessionId, 'catalog_product.update', array($_product['sku'], $product_details));
                    $clientiron->call($sessionId, 'product_stock.update', array($_product['sku'], $inventroy[0]));

                    $resultimg = null;
                    foreach ($newImages as $img) {

                        try {
                            $resultimg = $client->call($session, 'catalog_product_attribute_media.info', array($_product['sku'], 'file' => $img['file']));

                        } catch (exception $e) {

                        }
                        $mpath = $mediapath . "catalog/product" . $img['file'];
                        $flnm = end(explode("/", $img['file']));

                        // print_r ($resultimg);
                        if ($resultimg == null or $resultimg == "") {

                            $file = array(
                                'name' => $flnm,
                                'content' => base64_encode(file_get_contents($mpath)),
                                'mime' => 'image/jpeg'
                            );

                            $result1 = $clientiron->call($sessionId, 'catalog_product_attribute_media.create', array($_product['sku'], array('file' => $file, 'label' => $img['label'], 'position' => $img['position'], 'types' => $img['types'], 'exclude' => $img['disabled'])));


                        }

                    }


                    if ($result) {
                        $client->call($session, 'catalog_product.update', array($_product['sku'], array('syncpro' => 0)));
                        Mage::getSingleton('adminhtml/session')->addSuccess('Synchronization succesfully completed');
                        $this->_redirect('*/*/');
                    } else {
                        Mage::getSingleton('adminhtml/session')->addError('Synchronization unsuccesfull');
                        $this->_redirect('*/*/');
                    }


                } else {
                    $attributeSets = $clientiron->call($sessionId, 'product_attribute_set.list');
                    $attributeSet = current($attributeSets);
                    $result = $clientiron->call($sessionId, 'catalog_product.create', array('simple', $attributeSet['set_id'], $_product['sku'], $product_details));
                    $clientiron->call($sessionId, 'product_stock.update', array($_product['sku'], $inventroy[0]));

                    $resultimg = null;
                    foreach ($newImages as $img) {
                        try {
                            $resultimg = $client->call($session, 'catalog_product_attribute_media.info', array($_product['sku'], 'file' => $img['file']));

                        } catch (exception $e) {

                        }
                        $mpath = $mediapath . "catalog/product/" . $img['file'];
                        $flnm = end(explode("/", $img['file']));
                        // print_r ($resultimg);
                        if ($resultimg == null or $resultimg == "") {

                            $file = array(
                                'name' => $flnm,
                                'content' => base64_encode(file_get_contents($mpath)),
                                'mime' => 'image/jpeg'
                            );

                            $result1 = $clientiron->call($sessionId, 'catalog_product_attribute_media.create', array($_product['sku'], array('file' => $file, 'label' => $img['label'], 'position' => $img['position'], 'types' => $img['types'], 'exclude' => $img['disabled'])));


                        }

                    }


                    if ($result) {
                        //$_product->setSyncpro(0)->save();
                        $client->call($session, 'catalog_product.update', array($_product['sku'], array('syncpro' => 0)));
                        Mage::getSingleton('core/session')->addSuccess('Synchronization succesfully completed');
                        $this->_redirect('*/*/');

                    } else {
                        Mage::getSingleton('core/session')->addError('Synchronization unsuccesfull');
                        $this->_redirect('*/*/');
                    }


                }

            }

        }
    }

    public function getProductionCollection() {
        $prodCollection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('type')
            ->addAttributeToSelect('type_id')
            ->addAttributeToSelect('syncpro')
            ->addFieldToFilter('syncpro', array(1))
            ->setOrder('updated_at', 'desc');

        foreach($prodCollection as $product) {
            yield array('product_id' => $product->getId(), 'sku' => $product->getSku(), 'type' => $product->getTypeId(), 'set' => $product->getAttributeSetId());
        }
    }
}