<?php
class Int_Datasync_Block_Adminhtml_Datasync extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_datasync';
    $this->_blockGroup = 'datasync';
    $this->_headerText = Mage::helper('datasync')->__('Datasync Logs');
    $this->_addButtonLabel = Mage::helper('datasync')->__('Add Item');
    parent::__construct();
    $this->_removeButton('add');
  }
}