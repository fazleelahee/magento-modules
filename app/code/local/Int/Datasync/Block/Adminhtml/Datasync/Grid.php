<?php

class Int_Datasync_Block_Adminhtml_Datasync_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('datasyncGrid');
      $this->setDefaultSort('datasync_id');
      $this->setDefaultDir('DESC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('datasync/datasync')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('datasync_id', array(
          'header'    => Mage::helper('datasync')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'datasync_id',
      ));

      $this->addColumn('sync_from', array(
          'header'    => Mage::helper('datasync')->__('Sync From'),
          'align'     =>'left',
          'index'     => 'sync_from',
          'width'     => '150px',
          'type'      => 'options',
          'options'   =>array('PMT'=>'PMT','Production'=>'Production'),
      ));

      $this->addColumn('sync_to', array(
          'header'    => Mage::helper('datasync')->__('Sync To'),
          'align'     =>'left',
          'index'     => 'sync_to',
          'width'     => '150px',
          'type'      => 'options',
          'options'   =>array('Production'=>'Production','PMT'=>'PMT'),
      ));

      $this->addColumn('log_details', array(
          'header'    => Mage::helper('datasync')->__('Synced Product SKU'),
          'align'     =>'left',
          'index'     => 'log_details',
      ));

      $this->addColumn('created_time', array(
          'header'    => Mage::helper('datasync')->__('Synced Start Time'),
          'align'     =>'left',
          'index'     => 'created_time',
          'width'     => '160px',
      ));

      $this->addColumn('finish_time', array(
          'header'    => Mage::helper('datasync')->__('Synced Finish Time'),
          'align'     =>'left',
          'index'     => 'finish_time',
          'width'     => '160px',
      ));

		
		$this->addExportType('*/*/exportCsv', Mage::helper('datasync')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('datasync')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    // protected function _prepareMassaction()
    // {
    //     $this->setMassactionIdField('datasync_id');
    //     $this->getMassactionBlock()->setFormFieldName('datasync');

    //     $this->getMassactionBlock()->addItem('delete', array(
    //          'label'    => Mage::helper('datasync')->__('Delete'),
    //          'url'      => $this->getUrl('*/*/massDelete'),
    //          'confirm'  => Mage::helper('datasync')->__('Are you sure?')
    //     ));

      
    //     $statuses = Mage::getSingleton('datasync/status')->getOptionArray();

    //     array_unshift($statuses, array('label'=>'', 'value'=>''));
    //     $this->getMassactionBlock()->addItem('status', array(
    //          'label'=> Mage::helper('datasync')->__('Change status'),
    //          'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
    //          'additional' => array(
    //                 'visibility' => array(
    //                      'name' => 'status',
    //                      'type' => 'select',
    //                      'class' => 'required-entry',
    //                      'label' => Mage::helper('datasync')->__('Status'),
    //                      'values' => $statuses
    //                  )
    //          )
    //     ));
    //     return $this;
    // }

  public function getRowUrl($row)
  {
      //return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}