<?php

class Int_Datasync_Block_Adminhtml_Datasync_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'datasync';
        $this->_controller = 'adminhtml_datasync';
        
        $this->_updateButton('save', 'label', Mage::helper('datasync')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('datasync')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('datasync_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'datasync_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'datasync_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('datasync_data') && Mage::registry('datasync_data')->getId() ) {
            return Mage::helper('datasync')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('datasync_data')->getTitle()));
        } else {
            return Mage::helper('datasync')->__('Add Item');
        }
    }
}