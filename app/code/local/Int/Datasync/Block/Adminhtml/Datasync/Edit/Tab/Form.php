<?php

class Int_Datasync_Block_Adminhtml_Datasync_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('datasync_form', array('legend'=>Mage::helper('datasync')->__('Item information')));
     
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('datasync')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));

      $fieldset->addField('filename', 'file', array(
          'label'     => Mage::helper('datasync')->__('File'),
          'required'  => false,
          'name'      => 'filename',
	  ));
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('datasync')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('datasync')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('datasync')->__('Disabled'),
              ),
          ),
      ));
     
      $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('datasync')->__('Content'),
          'title'     => Mage::helper('datasync')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
          'required'  => true,
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getDatasyncData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getDatasyncData());
          Mage::getSingleton('adminhtml/session')->setDatasyncData(null);
      } elseif ( Mage::registry('datasync_data') ) {
          $form->setValues(Mage::registry('datasync_data')->getData());
      }
      return parent::_prepareForm();
  }
}