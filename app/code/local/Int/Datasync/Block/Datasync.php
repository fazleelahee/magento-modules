<?php
class Int_Datasync_Block_Datasync extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getDatasync()     
     { 
        if (!$this->hasData('datasync')) {
            $this->setData('datasync', Mage::registry('datasync'));
        }
        return $this->getData('datasync');
        
    }
}