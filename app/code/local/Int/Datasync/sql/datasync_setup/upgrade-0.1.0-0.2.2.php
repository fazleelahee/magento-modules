<?php

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$attributeInstaller = new Mage_Catalog_Model_Resource_Setup();
$entityTypeId = 'catalog_product';
$attributeCode ='blocked';

$attributeInstaller->addAttribute($entityTypeId, $attributeCode, array(
    'type'              => 'int',
    'label'             => 'Blocked',
    'input'             => 'boolean',
    'backend'           => 'eav/entity_attribute_backend_array',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => false,
    'user_defined'      => false,
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'unique'            => false,
    'default'           => 0
));

$installer->endSetup();