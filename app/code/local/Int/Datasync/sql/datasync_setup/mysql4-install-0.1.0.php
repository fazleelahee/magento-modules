<?php

$installer = $this;

$installer->startSetup();

$installer->run("

 DROP TABLE IF EXISTS {$this->getTable('datasync')};
CREATE TABLE {$this->getTable('datasync')} (
  `datasync_id` int(11) unsigned NOT NULL auto_increment,
  `sync_from` varchar(255) NOT NULL,
  `sync_to` varchar(255) NOT NULL,
  `log_details` longtext NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_time` datetime DEFAULT NULL,
  `finish_time` datetime DEFAULT NULL,
  PRIMARY KEY (`datasync_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 
