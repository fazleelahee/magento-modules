<?php

$installer = $this;

$installer->startSetup();

// $installer->run("

// -- DROP TABLE IF EXISTS {$this->getTable('commercialinvoice')};
// CREATE TABLE {$this->getTable('commercialinvoice')} (
//   `commercialinvoice_id` int(11) unsigned NOT NULL auto_increment,
//   `title` varchar(255) NOT NULL default '',
//   `filename` varchar(255) NOT NULL default '',
//   `content` text NOT NULL default '',
//   `status` smallint(6) NOT NULL default '0',
//   `created_time` datetime NULL,
//   `update_time` datetime NULL,
//   PRIMARY KEY (`commercialinvoice_id`)
// ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

//     ");

$attributeInstaller = new Mage_Catalog_Model_Resource_Setup();

$attributeInstaller->addAttribute('catalog_product', 'commercial_product_title', array(  
  'type'              => 'text',
  'label'             => 'commercial product title',
  'input'             => 'text',
  'class'             => '',
  'backend'           => 'eav/entity_attribute_backend_array',   
  'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
  'visible'           => true,
  'required'          => false,
  'user_defined'      => false,
  'default'           => '',
  'searchable'        => false,
  'filterable'        => false,
  'comparable'        => false,
  'visible_on_front'  => false,
  'unique'            => false
));

$installer->endSetup(); 
