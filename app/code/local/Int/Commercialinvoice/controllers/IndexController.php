<?php
class Int_Commercialinvoice_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
    	
		$this->loadLayout();     
		$this->renderLayout();
    }
    
     public function commerceinvoiceAction()
    {
	//echo "<pre>";
    	 $orderIds = (array)$this->getRequest()->getParam('order_ids');
	 
	$ids= explode(',', $orderIds[0]);
	 $count=count($ids);
	//print_r($orderIds);
	$i=0;
	foreach($ids as $id){
	  $i++;
	    if($i==$count)
	    {
		$oid=$id;

	    }
	      
	    }
	  $order=Mage::getModel('sales/order')->load($oid);
	  if ($order->hasInvoices()) {
	     $invIncrementId = array();
           foreach ($order->getInvoiceCollection() as $invoice) {
            $invoiceIncId[] = $invoice->getIncrementId();
     
	   }
	  
	  }else{
	   Mage::getSingleton('adminhtml/session')->addError(Mage::helper('commercialinvoice')->__('Invoice not created'));	      
	    $this->_redirectReferer(); 
	  }
	
    }
}
