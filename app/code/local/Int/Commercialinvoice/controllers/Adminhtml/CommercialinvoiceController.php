<?php

class Int_Commercialinvoice_Adminhtml_CommercialinvoiceController extends Mage_Adminhtml_Controller_action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('commercialinvoice/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
	$this->loadLayout();     
	$this->renderLayout();
	
	}
	
	public function commerceinvoiceAction()
    {
	//echo "<pre>";
    	 $orderIds = (array)$this->getRequest()->getParam('order_ids');
	 
	$ids= explode(',', $orderIds[0]);
	 $count=count($ids);
	//print_r($orderIds);
	$i=0;
	foreach($ids as $id){
	  $i++;
	    if($i==$count)
	    {
		$oid=$id;

	    }
	      
	    }
	  $order=Mage::getModel('sales/order')->load($oid);
	  if ($order->hasInvoices()) {
	     $invIncrementId = array();
           foreach ($order->getInvoiceCollection() as $invoice) {
            $invoiceIncId[] = $invoice->getIncrementId();
     
	   }
	    $this->_redirect('commercialinvoice/adminhtml_commercialinvoice/index'); 
	  }else{
	   Mage::getSingleton('adminhtml/session')->addError(Mage::helper('commercialinvoice')->__('Invoice not created'));	      
	    $this->_redirectReferer(); 
	  }
	
	
	
    }


}
