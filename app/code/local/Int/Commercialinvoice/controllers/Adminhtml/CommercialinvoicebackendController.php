<?php
class Int_Commercialinvoice_Adminhtml_CommercialinvoicebackendController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
    {
	$this->loadLayout();
	$this->_title($this->__("Backend Page Title"));
	$this->renderLayout();
    }
    
    	public function commerceinvoiceAction()
    {
	//echo "<pre>";
    	 $orderIds = (array)$this->getRequest()->getParam('order_ids');
	 
	$ids= explode(',', $orderIds[0]);
	 $count=count($ids);
	//print_r($orderIds);
	$i=0;
	foreach($ids as $id){
	  $i++;
	    if($i==$count)
	    {
		$oid=$id;

	    }
	      
	    }
  	$order=Mage::getModel('sales/order')->load($oid);
	$flag = '';
	if($order->hasInvoices()) {
	
 	foreach ($order->getInvoiceCollection() as $invoice) {
            $invoiceIncId = $invoice->getIncrementId();
            $invoive = Mage::getModel('sales/order_invoice')->loadByIncrementId($invoiceIncId);
            $invoiceId = $invoice->getId(); 
	
	$ordered_items = $invoive->getAllItems(); 
	foreach($ordered_items as $item){
	 $product = Mage::getModel('catalog/product')->load($item->getProductId());// echo '<pre>'; print_r($product->getData());
	 $product_title = $product->getCommercialProductTitle();  
	     if(isset($product_title))  { 
		$flag =1; } 
	     else { $flag = 0;  }
	 } 
	 if($flag==0){
	    $this->_redirect('admin_commercialinvoice/adminhtml_commercialinvoicebackend/index/oid/'.$oid);
		} else { 
	
	 $this->_redirect('admin_commercialinvoice/adminhtml_commercialinvoicebackend/createInvoice/invoiceid/'.$invoiceId); 
	 }
	    } 
	 	} 
	else{
	   Mage::getSingleton('adminhtml/session')->addError(Mage::helper('commercialinvoice')->__('Invoice not created'));	      
	    $this->_redirectReferer(); 
	  }
	
	
	
    }
    
      public function printcommerceAction()
    {
	$data=$this->getRequest()->getPost();
	$commname= $data['commrecename'];
	Mage::register("commercialinfo", $data['commrecename']);
	if ($invoice = Mage::getModel('sales/order_invoice')->load($data['invid'])) {
		foreach ($invoice->getAllItems() as $item) {
			 $pid=$item->getProductId();
			 $product=Mage::getModel('catalog/product')->load($pid);
			 $iid=$item->getId();
		foreach($commname as $itid=>$cname){
		     if($iid==$itid){
			$product->setCommercialProductTitle($cname)->save();
			
		     }
		
	             }
		
		 }
		//$classA = new Int_Commercialinvoice_Model_Order_Pdf_Invoice();
		//$pdf = $classA->getPdf(array($invoice));
                $pdf = Mage::getModel('northstar/order_pdf_invoice')->getPdf(array($invoice));
                $this->_prepareDownloadResponse('invoice'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').
                    '.pdf', $pdf->render(), 'application/pdf');
		 
            }
    
	
    }
	public function createInvoiceAction() { 
	
	$oid= $this->getRequest()->getParam('invoiceid'); 
	$invoice = Mage::getModel('sales/order_invoice')->load($oid);
	$commrcename = array();
	foreach($invoice->getAllItems() as $item) {
	$pid=$item->getProductId();
	$product=Mage::getModel('catalog/product')->load($pid);  
	$commrcename[$item->getId()] = $product->getCommercialProductTitle();                  
	} 

	Mage::register("commercialinfo", $commrcename);
	$pdf = Mage::getModel('northstar/order_pdf_invoice')->getPdf(array($invoice));
	$this->_prepareDownloadResponse('invoice'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').
	'.pdf', $pdf->render(), 'application/pdf');
	// $this->_redirect('admin/sales_order/index');
	}
}
