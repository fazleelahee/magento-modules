<?php

class Int_Commercialinvoice_Model_Commercialinvoice extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('commercialinvoice/commercialinvoice');
    }
}
