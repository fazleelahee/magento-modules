<?php
class Int_Commercialinvoice_Model_Observer
{
    public function addCommercialOption($observer)
    {
        $block = $observer->getEvent()->getBlock();
        if(get_class($block) =='Mage_Adminhtml_Block_Widget_Grid_Massaction'
            && $block->getRequest()->getControllerName() == 'sales_order')
        {
            $block->addItem('commercialinvoice', array(
                'label' => 'Commercial Invoice',
                'url' => Mage::app()->getStore()->getUrl('commercialinvoice/adminhtml_commercialinvoicebackend/commerceinvoice')
            ));
        }
    }
}
