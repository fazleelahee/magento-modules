<?php

class Int_Commercialinvoice_Model_Mysql4_Commercialinvoice extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the commercialinvoice_id refers to the key field in your database table.
        $this->_init('commercialinvoice/commercialinvoice', 'commercialinvoice_id');
    }
}
