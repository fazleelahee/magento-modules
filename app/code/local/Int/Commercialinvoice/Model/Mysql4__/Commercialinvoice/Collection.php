<?php

class Int_Commercialinvoice_Model_Mysql4_Commercialinvoice_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('commercialinvoice/commercialinvoice');
    }
}
