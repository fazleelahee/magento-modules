<?php

require_once 'Northstar/Invoice/Model/Order/Pdf/Invoice.php';
class Int_Commercialinvoice_Model_Order_Pdf_Invoice extends Northstar_Invoice_Model_Order_Pdf_Invoice
{
	
	protected function insertImage($image, $x1, $y1, $x2, $y2, $width, $height, &$page)
	{
		if (!is_null($image)) {
			try{
				$width = (int) $width;
				$height = (int) $height;

				//Get product image and resize it
				$imagePath = Mage::helper('catalog/image')->init($image, 'image')
					->keepAspectRatio(true)
					->keepFrame(false)
					->resize($width, $height)
					->__toString();

				if(preg_match('/index.php/',$imagePath)) {
				    $url = str_replace('index.php/', '',Mage::getBaseUrl())."index.php/";
				} else {
				    $url = str_replace('index.php/', '',Mage::getBaseUrl());
				}
				$imageLocation = str_replace($url,dirname(__FILENAME__)."/",$imagePath);

				$pdf_image = Zend_Pdf_Image::imageWithPath($imageLocation);
				//Draw image to PDF
				$page->drawImage($pdf_image, $x1, $y1, $x2, $y2);
			}
			catch (Exception $e) {
				return false;
			}
		}
	}

	public function getPdf($invoices = array())
	{
		$width = 1000;
		$height = 1000;
		$this->_beforeGetPdf();
		$this->_initRenderer('invoice');

		$commercialinfo=Mage::registry("commercialinfo");

		$pdf = new Zend_Pdf();
		$this->_setPdf($pdf);
		$style = new Zend_Pdf_Style();
		$this->_setFontBold($style, 9);

		foreach ($invoices as $invoice) {
			if ($invoice->getStoreId()) {
				Mage::app()->getLocale()->emulate($invoice->getStoreId());
			}
			$page = $pdf->newPage(Zend_Pdf_Page::SIZE_A4);
			$pdf->pages[] = $page;

			$order = $invoice->getOrder();



			$orderIncrementId = $order->getIncrementId();
			$orderId = Mage::getModel('sales/order')
             			->loadByIncrementId($orderIncrementId)
             			->getEntityId();


            $orderLoad = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
			$shippingAddress = $orderLoad->getShippingAddress();
			$customerCountryCode = $shippingAddress->getCountry();
			$customerCountryNameModel = Mage::getModel('directory/country')->loadByCode($customerCountryCode);
			$customerCountryName = $customerCountryNameModel->getName();



			/* Add image */
			//$this->insertLogo($page, $invoice->getStore());


			// $iconpath = Mage::getConfig()->getOptions()->getMediaDir().DS.'invoice/header-logo.png';			
			// $checkbox = Zend_Pdf_Image::imageWithPath($iconpath);	
			// $page->drawImage($checkbox, 25,780 , 125, 830);

			/* Add address */
			
			//$this->insertAddress($page, $invoice->getStore());	

			$today = date("d M, Y");

		$this->y = $this->y ? $this->y : 815;
		$top = $this->y;
	
		//$page->setFillColor(new Zend_Pdf_Color_GrayScale(0.45));
		//$page->setLineColor(new Zend_Pdf_Color_GrayScale(0.45));
		//$page->drawRectangle(25, $top, 570, $top - 55);
		//$page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
		//$this->setDocHeaderCoordinates(array(25, $top, 570, $top - 55));
		$this->_setFontBold($page, 14);
	
		//$this->y -= 10;
		
		$page->drawText('Commercial Invoice', 30, $this->y, 'UTF-8');


		 $top -= 10;
		 $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
		 //$page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
		 $page->setLineWidth(0.5);
		 $page->drawRectangle(25, $top, 574, ($top - 30));
		  $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

		 $this->_setFontBold($page, 10);
		$page->drawText('Airway Bill Number : ', 35, $this->y-25, 'UTF-8');

		// $page->drawRectangle(275, $top, 570, ($top - 25));
		 $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
		 $page->drawRectangle(25, $top-30, 208, ($top - 90));
		 $page->drawRectangle(208, $top-30, 391, ($top - 90));
		 $page->drawRectangle(391, $top-30, 574, ($top - 90));
		 $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

		 $page->drawText('Date of Export ', 35, $this->y-60, 'UTF-8');
		 $page->drawText('Ultimate Destination ', 215, $this->y-60, 'UTF-8');
		 $page->drawText('Number of Packages ', 400, $this->y-60, 'UTF-8');

		 $this->_setFontRegular($page, 10);

		 $anyDate = $order->getCreatedAt();
		 $dateTimestamp = Mage::getModel('core/date')->timestamp(strtotime($anyDate));
		 $currentDate = date('d M, Y', $dateTimestamp);
		 $page->drawText($currentDate, 35, $this->y-80, 'UTF-8');
		 $page->drawText($customerCountryName, 215, $this->y-80, 'UTF-8');

		 $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
		 $page->drawRectangle(25, $top-90, 208, ($top - 155));
		 $page->drawRectangle(208, $top-90, 391, ($top - 155));
		 $page->drawRectangle(391, $top-90, 574, ($top - 155));
		 $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

		 $this->_setFontBold($page, 10);
		 $page->drawText('Shippers Reference ', 35, $this->y-120, 'UTF-8');
		 $page->drawText('Importer Customers ID/EIN# ', 215, $this->y-120, 'UTF-8');
		 $page->drawText('Recipient Customs ID/EIN# ', 400, $this->y-120, 'UTF-8');

		 $this->_setFontRegular($page, 10);
		 $page->drawText($order->getCustomerId(), 400, $this->y-140, 'UTF-8');
		 $page->drawText($order->getIncrementId(), 35, $this->y-140, 'UTF-8');
		 $page->drawText('373137944', 215, $this->y-140, 'UTF-8');

		 $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
		 $page->drawRectangle(25, $top-155, 208, ($top - 305));
		 $page->drawRectangle(208, $top-155, 391, ($top - 305));
		 $page->drawRectangle(391, $top-155, 574, ($top - 305));
		 $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

		 $this->_setFontBold($page, 10);
		 $page->drawText('Shipper/Exporter ', 35, $this->y-185, 'UTF-8');
		 $page->drawText('Consignee ', 215, $this->y-185, 'UTF-8');
		 $page->drawText('Importer ', 400, $this->y-185, 'UTF-8');

		 $this->_setFontRegular($page, 10);
		  $page->drawText($this->insertShipperAddress($page, $invoice->getStore(), $order, $orderId), 35, $this->y-205, 'UTF-8');
		 $page->drawText($this->insertCustomStoreAddress($page, $invoice->getStore(), $order), 215, $this->y-205, 'UTF-8');



		 	$this->y =$top-320;

			/* Add table head */
			$this->_setFontBold($page, 9);
			//$page->setFillColor(new Zend_Pdf_Color_Rgb(0.4, 0.4, 0.4));
			$page->drawText('Country of', 25, $this->y, 'UTF-8');
			$page->drawText('Manufacture', 25, $this->y-10, 'UTF-8');
			$page->drawText('Description of Goods', 115, $this->y, 'UTF-8');
			$page->drawText('Weight', 300, $this->y, 'UTF-8');
			$page->drawText('Quantity', 350, $this->y, 'UTF-8');
			$page->drawText('Unit Value', 400, $this->y, 'UTF-8');
			$page->drawText('Commodity Value', 470, $this->y, 'UTF-8');
			//$page->drawText('Subtotal', 535, $this->y, 'UTF-8');
			
			$this->y -=20;

			$page->setLineWidth(0.5)
		  	     ->drawLine(25, $this->y, 574, $this->y);



		  	$i = 0;
		  	$totalWeight = 0;
		  	$grandTotal = 0;

		  	
			/* Add body */
			foreach ($invoice->getAllItems() as $item){
				
				$this->y -=20;
				if ($item->getOrderItem()->getParentItem()) {
					continue;
				}

				// if ($this->y < 15) {
				// 	$page = $this->newPage(array('table_header' => true));
				// }
				 //echo "<pre>";
				// print_r($item->getData());
				 //die();

				$this->_setFontRegular($page, 11);
				$productId = $item->getOrderItem()->getProductId();
				$_product = Mage::getModel('catalog/product')->load($productId);
				$manufacturer = $_product->getAttributeText('country_of_manufacture');
				$weight = $_product->getWeight();
				$totalWeight += $weight;
				$grandTotal += $item->getBaseRowTotal();
				$commodityPrice = (($item->getBasePrice() - $item->getBaseDiscountAmount()) * $item->getQty());

				 $pname = $commercialinfo[$item->getId()]; // $item->getName();
		

                $textChunk = wordwrap($pname, 35, "\n");

                //Added for product image
                // foreach(explode("\n", $textChunk) as $textLine)
                // {
                //     $this->y -= 20;

                //     if($textLine!=='') {
                //     $page->drawText(strip_tags(ltrim($textLine)), 100 , $this->y);
                //     //$page->drawText($item->getName(), 100, $this->y, 'UTF-8');
                //     //$contentTopFreeSpace -=14;
                //     //$this->y -= 14;
                //     }
                // }

                $countryCode = $this->getCountryCodeByName(trim($manufacturer));

                $gmToKg = ($weight/1000);

				$page->drawText($countryCode, 50, $this->y, 'UTF-8');
				$page->drawText($pname, 120, $this->y, 'UTF-8');
				$page->drawText(($gmToKg*1)."Kg", 305, $this->y, 'UTF-8');
				$page->drawText(($item->getQty()*1), 365, $this->y, 'UTF-8');
				$page->drawText(Mage::helper('core')->currency(($item->getBasePrice()*1), true, false), 405, $this->y, 'UTF-8');
				$page->drawText(Mage::helper('core')->currency(($commodityPrice*1), true, false), 475, $this->y, 'UTF-8');


				// die();

				 $i++;
				// $this->_setFontRegular($page, 9);

				// if ($item->getOrderItem()->getParentItem()) {
				// 	continue;
				// }



					
	
				// $iconpath = Mage::getConfig()->getOptions()->getMediaDir().DS.'invoice/box-icon.png';			
				// $checkbox = Zend_Pdf_Image::imageWithPath($iconpath);	
				// $page->drawImage($checkbox, 545,(int)$this->y + 35 , 560, (int)$this->y + 50);
			}

			$page->drawText($i, 400, 735, 'UTF-8');

			$this->y -=20;

			$page->setLineWidth(0.5)
		  	     ->drawLine(25, $this->y, 574, $this->y);

		  	$this->y -=20;
		  	$this->_setFontRegular($page, 10);
		  	$page->drawText("Total Weight: ", 230, $this->y, 'UTF-8');
		  	$page->drawText("Total Invoice Value: ", 365, $this->y, 'UTF-8');
		  	
		  	$this->_setFontBold($page, 10);
		  	$page->drawText(($totalWeight/1000)."Kg ", 305, $this->y, 'UTF-8');
		  	$page->drawText(Mage::helper('core')->currency(($order->getBaseTotalPaid()*1), true, false), 475, $this->y, 'UTF-8');

		
		  	 $top = $this->y-20;

		  	 $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
			 $page->drawRectangle(25, $top, 275, ($top - 60));
			 $page->drawRectangle(275, $top, 574, ($top - 60));
			 $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

			 $page->drawText('Declared Currency ', 100, $this->y-40, 'UTF-8');
			 $this->_setFontBold($page, 12);
		 	 $page->drawText('Terms Of Sale ', 390, $this->y-40, 'UTF-8');

		 	 $this->_setFontRegular($page, 10);
		 	 $page->drawText('GBP Pound Sterling', 100, $this->y-60, 'UTF-8');

		 	 $top = $this->y-55;
		 	 $this->y = $top;

		 	 $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
			 $page->drawRectangle(360, $top, 370, ($top - 10));
			 $page->drawRectangle(420, $top, 430, ($top - 10));
			 $page->drawRectangle(480, $top, 490, ($top - 10));
			 $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

			 $tickIconPath = Mage::getConfig()->getOptions()->getMediaDir().DS.'invoice/icon-tick.png';
			 $blankIconPath = Mage::getConfig()->getOptions()->getMediaDir().DS.'invoice/icon-blank.png';			
			 $checkbox = Zend_Pdf_Image::imageWithPath($tickIconPath);	
			 $page->drawImage($checkbox, 420, $top, 430, ($top - 10));

			 $BlankCheckbox = Zend_Pdf_Image::imageWithPath($blankIconPath);	
			 $page->drawImage($BlankCheckbox, 360, $top, 370, ($top - 10));
			 $page->drawImage($BlankCheckbox, 480, $top, 490, ($top - 10));

			 $this->_setFontRegular($page, 10);
			 $page->drawText('FOB', 380, $top-8, 'UTF-8');
			 $page->drawText('CIF', 440, $top-8, 'UTF-8');
			 $page->drawText('C&F', 500, $top-8, 'UTF-8');

			 $top = $this->y-40;
			 $this->y = $top;

			 $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
			 $page->drawRectangle(25, $top, 574, ($top - 100));
			 $page->drawRectangle(40, $top-30, 48, ($top - 38));
			 $page->drawRectangle(40, $top-74, 48, ($top - 82));
			 $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

			 $page->drawImage($checkbox, 40, $top-30, 48, ($top - 38));
			 $page->drawImage($checkbox, 40, $top-74, 48, ($top - 82));

			 $page->drawText('TOXIC SUBSTANCE CONTROL ACT DECLARATION', 35, $top-20, 'UTF-8');
			 $page->drawText('"I certify that all chemical substances in this shipment comply with all applicable rules or orders under ', 56, $top-38, 'UTF-8');
			 $page->drawText('TSCA and that I am not offering a chemical substance for entry in violation of TSCA or any applicable rule ', 56, $top-50, 'UTF-8');
			 $page->drawText('or order under TSCA." ', 56, $top-62, 'UTF-8');
			 $page->drawText('"I certify that all chemicals in this shipment are not subject to TSCA."', 56, $top-82, 'UTF-8');
		 	 
			 $this->y = $top-120;

			 $page->drawText('Signature of Shipper/Exporter', 25, $this->y, 'UTF-8');
			 $page->drawText('I declare that all the information contained in this invoice is true and correct.', 25, $this->y-20, 'UTF-8');

			 $this->_setFontBold($page, 11);
			 $page->drawText('Date: ', 25, $this->y-40, 'UTF-8');
			 $page->drawText('Signed: ', 170, $this->y-40, 'UTF-8');

			 $this->_setFontRegular($page, 11);
			 $page->drawText($today, 60, $this->y-40, 'UTF-8');

		 	 $this->y -= 40;

		  	if ($this->y < 20) 
		  	{
				$page = $this->newPage(array('table_header' => false));
			}
			
			//$this->insertCustomStoreAddress($page, $invoice->getStore(), $order, $nextPageHeightCount);
		}
		$this->_afterGetPdf();

		return $pdf;
	}

	public function newPage(array $settings = array())
	{
		/* Add new table head */
		$page = $this->_getPdf()->newPage(Zend_Pdf_Page::SIZE_A4);
		$this->_getPdf()->pages[] = $page;
		$this->y = 800;

		if (!empty($settings['table_header'])) {
			$this->_setFontRegular($page);
			//$page->setFillColor(new Zend_Pdf_Color_RGB(0.93, 0.92, 0.92));
			//$page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
			//$page->setLineWidth(0.5);
			//$page->drawRectangle(25, $this->y, 570, $this->y-15);
			$this->y -=10;
			/* Add table head */
			$this->_setFontBold($page, 9);
			//$page->setFillColor(new Zend_Pdf_Color_Rgb(0.4, 0.4, 0.4));
			$page->drawText('Country of', 25, $this->y, 'UTF-8');
			$page->drawText('Manufacture', 25, $this->y-10, 'UTF-8');
			$page->drawText('Description of Goods', 115, $this->y, 'UTF-8');
			$page->drawText('Weight', 300, $this->y, 'UTF-8');
			$page->drawText('Quantity', 350, $this->y, 'UTF-8');
			$page->drawText('Unit Value', 400, $this->y, 'UTF-8');
			$page->drawText('Commodity Value', 470, $this->y, 'UTF-8');
			//$page->drawText('Subtotal', 535, $this->y, 'UTF-8');
			
			$this->y -=20;

			$page->setLineWidth(0.5)
		  	     ->drawLine(25, $this->y, 574, $this->y);


			$page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
			$this->y -=20;
		}

		return $page;
	}


	protected function insertOrder(&$page, $obj, $putOrderId = true)
	{
	
    }

   
	protected function insertCustomStoreAddress(&$page, $store = null, $obj)
	{
		if ($obj instanceof Mage_Sales_Model_Order) {
		    $shipment = null;
		    $order = $obj;
		} elseif ($obj instanceof Mage_Sales_Model_Order_Shipment) {
		    $shipment = $obj;
		    $order = $shipment->getOrder();
		}
		

		/*
		
		$page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
		$font = $this->_setFontRegular($page, 9);
		$page->setLineWidth(0.8);
		$this->y = $this->y ? $this->y : 815;
		$top = $this->y;
		$top = 95;
		$this->_setFontBold($page, 10);
		$page->drawText(Mage::helper('sales')->__('Everyone Does IT'),335, 95, 'UTF-8');
		$this->_setFontRegular($page, 8);
		
		/*foreach (explode("\n", Mage::getStoreConfig('sales/identity/address', $store)) as $value){
			if ($value !== '') {
				//$value = preg_replace('/]*>/i', "\n", $value);
				$page->drawText($value, 300, ($top -= 15), 'UTF-8');
				//$page->drawText($value, 25, ($top -= 10), 'UTF-8');
			}
		}*/

		
		
		// foreach (explode("\n", Mage::getStoreConfig('sales/identity/address', $store)) as $value){
		// 	if ($value !== '') {
		// 	    $value = preg_replace('/<br[^>]*>/i', "\n", $value);
		// 	    foreach (Mage::helper('core/string')->str_split($value, 45, true, true) as $_value) {
		// 		$page->drawText(trim(strip_tags($_value)),
		// 		    335,
		// 		    $top -= 10,
		// 		    'UTF-8');
		// 	    }
		// 	}
		// }
		

		
		
		//$this->y = ($this->y > $top) ? $top : $this->y;
		
		
		//$page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
		
		
		if (!$order->getIsVirtual()) {
		    /* Shipping Address */
		    $shippingAddress = $this->_formatAddress($order->getShippingAddress()->format('pdf'));
		    $shippingMethod  = $order->getShippingDescription();
		}
		
		if (!$order->getIsVirtual()) {
			$top = 625;
			$us = 0;
			foreach ($shippingAddress as $value){
				$this->_setFontRegular($page, 9);
			    if ($value!=='') {
				$text = array();
				foreach (Mage::helper('core/string')->str_split($value, 45, true, true) as $_value) {
				    $text[] = $_value;
				}
				foreach ($text as $part) {
					
					if(strip_tags(ltrim($part)) == 'United States'){ $us = 1; }
					
					$page->drawText(strip_tags(ltrim($part)), 215, ($top -= 12), 'UTF-8');

				}
			    }
			}
			$page->drawText(strip_tags("Email: ".$order->getCustomerEmail()), 215, ($top -= 12), 'UTF-8');
		}	
	}


	protected function insertShipperAddress(&$page, $store = null, $obj, $orderId)
	{
		if ($obj instanceof Mage_Sales_Model_Order) {
		    $shipment = null;
		    $order = $obj;
		} elseif ($obj instanceof Mage_Sales_Model_Order_Shipment) {
		    $shipment = $obj;
		    $order = $shipment->getOrder();
		}



		$ordercollection = Mage::getModel('sales/order_item')->getCollection();//->addFieldToFilter('order_id',$orderId);

		$ordercollection = $ordercollection->addFieldToFilter('main_table.order_id', array('eq' => $orderId));


		$warehouseAddressCollection = $ordercollection->getSelect()
			 			    ->join(
			 			    array('warehouse_address'=>d1g3_northstar_multiwarehouse_warehouse),
			 			    'warehouse_address.entity_id = main_table.warehouse_id',
			 			    array(
			 			        'warehouse_address.*'
			 			    )
			 			    );


		$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
				
		$rowArray =$connection->fetchRow($warehouseAddressCollection);   //return row
		
		
		$page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
		$font = $this->_setFontRegular($page, 9);

		$top = 625;
		
		// foreach (explode("\n", Mage::getStoreConfig('sales/identity/address', $store)) as $value){
		// 	if ($value !== '') {
		// 	    $value = preg_replace('/<br[^>]*>/i', "\n", $value);
		// 	    foreach (Mage::helper('core/string')->str_split($value, 35, true, true) as $_value) {
		// 		$page->drawText(trim(strip_tags($_value)),
		// 		    35,
		// 		    $top -= 12,
		// 		    'UTF-8');
		// 	    }
		// 	}
		// }

		// $page->drawText('Giftware Supplies', 35, $top -= 12, 'UTF-8');
		// $page->drawText('Unit D1, Phoenix Industrial Estate', 35, $top -= 12, 'UTF-8');
		// $page->drawText('Rosslyn Crescent', 35, $top -= 12, 'UTF-8');
		// $page->drawText('HA1 2SP', 35, $top -= 12, 'UTF-8');
		// $page->drawText('UK', 35, $top -= 12, 'UTF-8');

		$page->drawText($rowArray['warehouse_name'], 35, $top -= 12, 'UTF-8');
		$page->drawText($rowArray['street1'], 35, $top -= 12, 'UTF-8');
		$page->drawText($rowArray['street2'], 35, $top -= 12, 'UTF-8');
		$page->drawText($rowArray['city'], 35, $top -= 12, 'UTF-8');
		$page->drawText($rowArray['state'], 35, $top -= 12, 'UTF-8');
		$page->drawText($rowArray['zipcode'], 35, $top -= 12, 'UTF-8');
		$page->drawText($rowArray['country'], 35, $top -= 12, 'UTF-8');
			
	}


	    /**
     * Set font as regular
     *
     * @param  Zend_Pdf_Page $object
     * @param  int $size
     * @return Zend_Pdf_Resource_Font
     */
    protected function _setFontRegular($object, $size = 7)
    {
        $font = Zend_Pdf_Font::fontWithPath(Mage::getBaseDir() . '/lib/MontserratFont/Montserrat-Light.ttf');
        $object->setFont($font, $size);
        return $font;
    }

    /**
     * Set font as bold
     *
     * @param  Zend_Pdf_Page $object
     * @param  int $size
     * @return Zend_Pdf_Resource_Font
     */
    protected function _setFontBold($object, $size = 7)
    {
        $font = Zend_Pdf_Font::fontWithPath(Mage::getBaseDir() . '/lib/MontserratFont/Montserrat-Regular.ttf');
        $object->setFont($font, $size);
        return $font;
    }


        /**
     * Insert address to pdf page
     *
     * @param Zend_Pdf_Page $page
     * @param null $store
     */
    protected function insertAddress(&$page, $store = null)
    {
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $font = $this->_setFontRegular($page, 8);
        $page->setLineWidth(0);
        $this->y = $this->y ? $this->y : 815;
        $top = 815;
        foreach (explode("\n", Mage::getStoreConfig('sales/identity/address', $store)) as $value){
            if ($value !== '') {
                $value = preg_replace('/<br[^>]*>/i', "\n", $value);
                foreach (Mage::helper('core/string')->str_split($value, 45, true, true) as $_value) {
                    $page->drawText(trim(strip_tags($_value)),
                        $this->getAlignRight($_value, 130, 440, $font, 8),
                        $top,
                        'UTF-8');
                    $top -= 10;
                }
            }
        }
        $this->y = ($this->y > $top) ? $top : $this->y;
    }

        /**
     * Draw Item process
     *
     * @param  Varien_Object $item
     * @param  Zend_Pdf_Page $page
     * @param  Mage_Sales_Model_Order $order
     * @return Zend_Pdf_Page
     */
    protected function _drawItem(Varien_Object $item, Zend_Pdf_Page $page, Mage_Sales_Model_Order $order)
    {   
        $orderItem = $item->getOrderItem();
        $type = $orderItem->getProductType();
        $renderer = $this->_getRenderer($type);

        $this->renderItem($item, $page, $order, $renderer);
        $this->_setFontRegular($page, 5);
        $transportObject = new Varien_Object(array('renderer_type_list' => array()));
        Mage::dispatchEvent('pdf_item_draw_after', array(
            'transport_object' => $transportObject,
            'entity_item'      => $item
        ));

        foreach ($transportObject->getRendererTypeList() as $type) {
            $renderer = $this->_getRenderer($type);
            if ($renderer) {
                $this->renderItem($orderItem, $page, $order, $renderer);
            }
        }

        return $renderer->getPage();
    }

    public function getCountryCodeByName($countryName)
    {
    	$countryId = '';
		$countryCollection = Mage::getModel('directory/country')->getCollection();
		foreach ($countryCollection as $country) {
		    if ($countryName == $country->getName()) {
		        $countryId = $country->getCountryId();
		        break;
		    }
		}
		return $countryId;
    }


}
