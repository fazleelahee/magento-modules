<?php

require_once 'Mage/Adminhtml/Block/Sales/Order/Grid.php';
class Int_Commercialinvoice_Block_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
 protected function _prepareMassaction()
    {
        parent::_prepareMassaction();
         
        // Append new mass action option
        $this->getMassactionBlock()->addItem(
            'newmodule',
            array('label' => $this->__('Commercial Invoice'),
                  'url'   => $this->getUrl('commercialinvoice/adminhtml_commercialinvoicebackend/commerceinvoice') //this should be the url where there will be mass operation
            )
        );
    }

}
