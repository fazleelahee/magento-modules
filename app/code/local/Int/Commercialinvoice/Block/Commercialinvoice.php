<?php
class Int_Commercialinvoice_Block_Commercialinvoice extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getCommercialinvoice()     
     { 
        if (!$this->hasData('commercialinvoice')) {
            $this->setData('commercialinvoice', Mage::registry('commercialinvoice'));
        }
        return $this->getData('commercialinvoice');
        
    }
}
