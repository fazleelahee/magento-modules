<?php
class Int_Commercialinvoice_Block_Adminhtml_Commercialinvoice extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_commercialinvoice';
    $this->_blockGroup = 'commercialinvoice';
    $this->_headerText = Mage::helper('commercialinvoice')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('commercialinvoice')->__('Add Item');
    parent::__construct();
  }
}
