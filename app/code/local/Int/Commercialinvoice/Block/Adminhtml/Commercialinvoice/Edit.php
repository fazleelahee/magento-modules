<?php

class Int_Commercialinvoice_Block_Adminhtml_Commercialinvoice_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'commercialinvoice';
        $this->_controller = 'adminhtml_commercialinvoice';
        
        $this->_updateButton('save', 'label', Mage::helper('commercialinvoice')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('commercialinvoice')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('commercialinvoice_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'commercialinvoice_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'commercialinvoice_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('commercialinvoice_data') && Mage::registry('commercialinvoice_data')->getId() ) {
            return Mage::helper('commercialinvoice')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('commercialinvoice_data')->getTitle()));
        } else {
            return Mage::helper('commercialinvoice')->__('Add Item');
        }
    }
}
