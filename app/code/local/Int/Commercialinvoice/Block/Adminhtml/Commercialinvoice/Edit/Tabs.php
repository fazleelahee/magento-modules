<?php

class Int_Commercialinvoice_Block_Adminhtml_Commercialinvoice_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('commercialinvoice_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('commercialinvoice')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('commercialinvoice')->__('Item Information'),
          'title'     => Mage::helper('commercialinvoice')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('commercialinvoice/adminhtml_commercialinvoice_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}
