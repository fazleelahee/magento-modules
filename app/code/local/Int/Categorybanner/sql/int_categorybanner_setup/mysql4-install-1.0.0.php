<?php

$bannerInstaller = $this;
$bannerInstaller->startSetup();

//$bannerInstaller = new Mage_Sales_Model_Mysql4_Setup();

$arg_attribute = 'category_banner';

$attribute  = array(
    'group'                     => 'General',
    'input'                     => 'multiselect',
    'type'                      => 'text',
    'label'                     => 'Banner Selection',
    'global'                    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'backend'                   => 'eav/entity_attribute_backend_array',
    'visible'                   => 1,
    'required'                  => 0,
    'visible_on_front'          => 0,
    'is_html_allowed_on_front'  => 0,
    'is_configurable'           => 0,
    'searchable'                => 0,
    'filterable'                => 0,
    'comparable'                => 0,
    'unique'                    => false,
    'user_defined'              => false,
    'default'                   => '0',
    'is_user_defined'           => false,
    'used_in_product_listing'   => true
);

$bannerInstaller->addAttribute('catalog_category', $arg_attribute, $attribute);
$attr_model = Mage::getModel('catalog/resource_eav_attribute');
$attr = $attr_model->loadByCode('catalog_category', $arg_attribute);
$attr_id = $attr->getAttributeId();
$option['attribute_id'] = $attr_id;
$option['value']['Slider'][0] = 'Slider';
$option['value']['Image'][0]  = 'Image';
$option['value']['Background Image'][0] = 'Background Image';
$option['value']['Masonry'][0] = 'Masonry';

$bannerInstaller->addAttributeOption($option);

$attribute  = array(
    'group'                     => 'General',
    'input'                     => 'image',
    'type'                      => 'varchar',
    'label'                     => 'Background Image',
    'global'                    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'backend'                   => 'catalog/category_attribute_backend_image',
    'visible'                   => 1,
    'required'                  => 0,
    'visible_on_front'          => 0,
    'is_html_allowed_on_front'  => 0,
    'is_configurable'           => 0,
    'searchable'                => 0,
    'filterable'                => 0,
    'comparable'                => 0,
    'unique'                    => false,
    'user_defined'              => false,
    'default'                   => '0',
    'is_user_defined'           => false,
    'used_in_product_listing'   => true,

);

$bannerInstaller->addAttribute('catalog_category', 'category_background_image', $attribute);


$attribute  = array(
    'group'                     => 'General',
    'input'                     => 'text',
    'type'                      => 'int',
    'label'                     => 'Masonry Block ID',
    'global'                    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'                   => 1,
    'required'                  => 0,
    'visible_on_front'          => 0,
    'is_html_allowed_on_front'  => 0,
    'is_configurable'           => 0,
    'searchable'                => 0,
    'filterable'                => 0,
    'comparable'                => 0,
    'unique'                    => false,
    'user_defined'              => false,
    'default'                   => '0',
    'is_user_defined'           => false,
    'used_in_product_listing'   => true,

);

$bannerInstaller->addAttribute('catalog_category', 'masonry_block_id', $attribute);

$attribute  = array(
    'group'                     => 'General',
    'input'                     => 'text',
    'type'                      => 'varchar',
    'label'                     => 'Slider Block (static cms block)',
    'global'                    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'                   => 1,
    'required'                  => 0,
    'visible_on_front'          => 0,
    'is_html_allowed_on_front'  => 0,
    'is_configurable'           => 0,
    'searchable'                => 0,
    'filterable'                => 0,
    'comparable'                => 0,
    'unique'                    => false,
    'user_defined'              => false,
    'default'                   => '0',
    'is_user_defined'           => false,
    'used_in_product_listing'   => true,

);

$bannerInstaller->addAttribute('catalog_category', 'slider_block_static', $attribute);



$bannerInstaller->endSetup();